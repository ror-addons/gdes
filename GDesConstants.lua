
	--Define the constant values that can be used in the other LUA scripts of the addon.
	GDesConstants = {}

	GDesConstants.ALERTS_DISABLED   = 1
	GDesConstants.CAST_SUCCESS = 1
	GDesConstants.CAST_FAILED  = 2
	GDesConstants.COMMS_ADDON       = 1
	GDesConstants.COMMS_AREA_ID     = 2
	GDesConstants.COMMS_STATUS        = 3
	GDesConstants.COMMS_CASTER_ID   = 4
	GDesConstants.COMMS_CASTER_NAME = 5
	GDesConstants.COMMS_TARGET_ID   = 6
	GDesConstants.COMMS_TARGET_NAME = 7
	GDesConstants.EXTRAS_TARGET_HEALTH = 1
	GDesConstants.EXTRAS_TARGET_NAME   = 2
	GDesConstants.EXTRAS_TARGET_CAREER = 3
	GDesConstants.FOREGROUND_HILIGHT = 1
	GDesConstants.FOREGROUND_TEXTBG  = 2
	GDesConstants.FOREGROUND_TEXTDC  = 3
	GDesConstants.FOREGROUND_SKULL   = 4
	GDesConstants.FOREGROUND_BLANK   = 5
	GDesConstants.ICON_REFKEY = "ReferenceKey"
	GDesConstants.TAB_GENERAL_ID = 1
	GDesConstants.TAB_COLOUR_ID  = 2
	GDesConstants.TAB_EXTRAS_ID  = 3
	GDesConstants.MAX_ICONS = 5
	
	GDesConstants.Anchors  = {
	  { name = "Top"   , opposite = "Bottom" , offsetX = 0  , offsetY = 5}
	, { name = "Left"  , opposite = "Right"  , offsetX = -2 , offsetY = 0}
	, { name = "Center", opposite = "Center" , offsetX = 0  , offsetY = 0}
	, { name = "Right" , opposite = "Left"   , offsetX = 0  , offsetY = 0}
	, { name = "Bottom", opposite = "Top"    , offsetX = 0  , offsetY = 5}
	                         }

	GDesConstants.Comms = {}
	GDesConstants.Comms.Channels = { [1] = "KageOrder" , [2] = "KageDestro" }

	GDesConstants.Features = {
	  [GDesConstants.EXTRAS_TARGET_HEALTH] = { window = "TargetHealth" , name = "Target Health" }
	, [GDesConstants.EXTRAS_TARGET_NAME]   = { window = "TargetName"   , name = "Target Name"   }
	, [GDesConstants.EXTRAS_TARGET_CAREER] = { window = "CareerIcon"   , name = "Career Icon"   }
	                         }

	GDesConstants.IconsBackgrounds = {
		  { texture = "GDesBackTexture" , slice = "Grey"   , name = "Grey"  }
		, { texture = "GDesBackTexture" , slice = "Orange" , name = "Orange"}
		, { texture = "GDesBackTexture" , slice = "Yellow" , name = "Yellow"}
		, { texture = "GDesBackTexture" , slice = "Green"  , name = "Green" }
		, { texture = "GDesBackTexture" , slice = "Red"    , name = "Red"   }
		, { texture = "GDesBackTexture" , slice = "Blue"   , name = "Blue"  }
		, { texture = "GDesBackTexture" , slice = "Purple" , name = "Purple"}
		, { texture = "GDesBackTexture" , slice = "Pink"   , name = "Pink"  }
		, { texture = "GDesBackTexture" , slice = "Black"  , name = "Black" }
		                             }

	GDesConstants.IconsForegrounds = {
		  { texture = "GDesForeTexture" , slice = "Hilight" , name = "Hilight"}
		, { texture = "GDesForeTexture" , slice = "TextBG"  , name = "TextBG" }
		, { texture = "GDesForeTexture" , slice = "TextDC"  , name = "TextDC" }
		, { texture = "GDesForeTexture" , slice = "Skull"   , name = "Skull"  }
		, { texture = "GDesForeTexture" , slice = "Blank"   , name = "Blank"  }
		                             }

	GDesConstants.Alerts = {    
	  { id = nil                                                   , name = "Do not alert" }
	, { id = SystemData.AlertText.Types.DEFAULT                    , name = "Default" }
	, { id = SystemData.AlertText.Types.COMBAT                     , name = "Combat" }
	, { id = SystemData.AlertText.Types.QUEST_NAME                 , name = "Quest Name" }
	, { id = SystemData.AlertText.Types.QUEST_CONDITION            , name = "Quest Condition" }
	, { id = SystemData.AlertText.Types.QUEST_END                  , name = "Quest End" }
	, { id = SystemData.AlertText.Types.OBJECTIVE                  , name = "Objective" }
	, { id = SystemData.AlertText.Types.RVR                        , name = "RvR" }
	, { id = SystemData.AlertText.Types.SCENARIO                   , name = "Scenario" }
	, { id = SystemData.AlertText.Types.MOVEMENT_RVR               , name = "Movement RvR" }
	, { id = SystemData.AlertText.Types.ENTERAREA                  , name = "Enter Area" }
	, { id = SystemData.AlertText.Types.STATUS_ERRORS              , name = "Status Errors" }
	, { id = SystemData.AlertText.Types.STATUS_ACHIEVEMENTS_GOLD   , name = "Status Achievements Gold"   }
	, { id = SystemData.AlertText.Types.STATUS_ACHIEVEMENTS_PURPLE , name = "Status Achievements Purple" }
	, { id = SystemData.AlertText.Types.STATUS_ACHIEVEMENTS_RANK   , name = "Status Achievements Rank"   }
	, { id = SystemData.AlertText.Types.STATUS_ACHIEVEMENTS_RENOUN , name = "Status Achievements Renown" }
	, { id = SystemData.AlertText.Types.PQ_ENTER                   , name = "PQ Enter" }
	, { id = SystemData.AlertText.Types.PQ_NAME                    , name = "PQ Name" }
	, { id = SystemData.AlertText.Types.PQ_DESCRIPTION             , name = "PQ Description" }
	, { id = SystemData.AlertText.Types.ENTERZONE                  , name = "Enter Zone" }
	, { id = SystemData.AlertText.Types.ORDER                      , name = "Order" }
	, { id = SystemData.AlertText.Types.DESTRUCTION                , name = "Destruction" }
	, { id = SystemData.AlertText.Types.NEUTRAL                    , name = "Neutral" }
	, { id = SystemData.AlertText.Types.ABILITY                    , name = "Ability" }
	, { id = SystemData.AlertText.Types.BO_ENTER                   , name = "BO Enter" }
	, { id = SystemData.AlertText.Types.BO_NAME                    , name = "BO Name" }
	, { id = SystemData.AlertText.Types.BO_DESCRIPTION             , name = "BO Description" }
	, { id = SystemData.AlertText.Types.ENTER_CITY                 , name = "Enter City" }
	, { id = SystemData.AlertText.Types.CITY_RATING                , name = "City Rating" }
	, { id = SystemData.AlertText.Types.GUILD_RANK                 , name = "Guild Rank" }
	, { id = SystemData.AlertText.Types.RRQ_UNPAUSED               , name = "RRQ Unpaused" }
	, { id = SystemData.AlertText.Types.LARGE_ORDER                , name = "Large Order" }
	, { id = SystemData.AlertText.Types.LARGE_DESTRUCTION          , name = "Large Destruction" }
	, { id = SystemData.AlertText.Types.LARGE_NEUTRAL              , name = "Large Neutral" }
	                         }		

	GDesConstants.SoundTypes = {
	  { id = nil                                           , name = "Do not play a sound"}
	, { id = GameData.Sound.ACTION_FAILED                  , name = "Action Failed"}
	, { id = GameData.Sound.ADVANCE_RANK                   , name = "Advance Rank"}
	, { id = GameData.Sound.ADVANCE_TIER                   , name = "Advance Tier"}
	, { id = GameData.Sound.APOTHECARY_ADD_FAILED          , name = "Apothecary Add Failed"}
	, { id = GameData.Sound.APOTHECARY_BREW_STARTED        , name = "Apothecary Brew Started"}
	, { id = GameData.Sound.APOTHECARY_CONTAINER_ADDED     , name = "Apothecary Container Added"}
	, { id = GameData.Sound.APOTHECARY_DETERMINENT_ADDED   , name = "Apothecary Determinent Added"}
	, { id = GameData.Sound.APOTHECARY_FAILED              , name = "Apothecary Failed"}
	, { id = GameData.Sound.APOTHECARY_ITEM_REMOVED        , name = "Apothecary Item Removed"}
	, { id = GameData.Sound.APOTHECARY_RESOURCE_ADDED      , name = "Apothecary Resource Added"}
	, { id = GameData.Sound.BETA_WARNING                   , name = "Beta Warning"}
	, { id = GameData.Sound.BUTTON_CLICK                   , name = "Button Click"}
	, { id = GameData.Sound.BUTTON_OVER                    , name = "Button Over"}
	, { id = GameData.Sound.CULTIVATING_HARVEST_CROP       , name = "Cultivating Harvest Crop"}
	, { id = GameData.Sound.CULTIVATING_NUTRIENT_ADDED     , name = "Cultivating Nutrient Added"}
	, { id = GameData.Sound.CULTIVATING_SEED_ADDED         , name = "Cultivating Seed Added"}
	, { id = GameData.Sound.CULTIVATING_SOIL_ADDED         , name = "Cultivating Soil Added"}
	, { id = GameData.Sound.CULTIVATING_WATER_ADDED        , name = "Cultivating Water Added"}
	, { id = GameData.Sound.ICON_CLEAR                     , name = "Icon Clear"}
	, { id = GameData.Sound.ICON_DROP                      , name = "Icon Drop"}
	, { id = GameData.Sound.ICON_PICKUP                    , name = "Icon Pickup"}
	, { id = GameData.Sound.LOOT_MONEY                     , name = "Loot Money"}
	, { id = GameData.Sound.MONETARY_TRANSACTION           , name = "Monetary Transaction"}
	, { id = GameData.Sound.OBJECTIVE_CAPTURE              , name = "Objective Capture"}
	, { id = GameData.Sound.OBJECTIVE_LOSE                 , name = "Objective Lose"}
	, { id = GameData.Sound.PREGAME_PLAY_GAME_BUTTON       , name = "Pregame Play Game Button"}
	, { id = GameData.Sound.PUBLIC_TOME_UNLOCKED           , name = "Public Tome Unlocked"}
	, { id = GameData.Sound.QUEST_ABANDONED                , name = "Quest Abandoned"}
	, { id = GameData.Sound.QUEST_ACCEPTED                 , name = "Quest Accepted"}
	, { id = GameData.Sound.QUEST_COMPLETED                , name = "Quest Completed"}
	, { id = GameData.Sound.QUEST_OBJECTIVES_COMPLETED     , name = "Quest Objective Complete"}
	, { id = GameData.Sound.RESPAWN                        , name = "Respawn"}
	, { id = GameData.Sound.RVR_FLAG_OFF                   , name = "RvR Flag Off"}
	, { id = GameData.Sound.RVR_FLAG_ON                    , name = "RvR Flag On"}
	, { id = GameData.Sound.TARGET_DESELECT                , name = "Target Deselect"}
	, { id = GameData.Sound.TARGET_SELECT                  , name = "Target Select"}
	, { id = GameData.Sound.TOME_TURN_PAGE                 , name = "Tome Turn Page"}
	, { id = GameData.Sound.WINDOW_CLOSE                   , name = "Window Close"}
	, { id = GameData.Sound.WINDOW_OPEN                    , name = "Window Open"}
	, { id = GameData.Sound.CLOSE_WORLD_MAP                , name = "World Map Close"}
	, { id = GameData.Sound.OPEN_WORLD_MAP                 , name = "World Map Open"}	          
	                          }							 