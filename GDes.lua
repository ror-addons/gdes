local VERSION = 1.2	-- Set version of app for future changes/comparisons.

local function printMsg(str)
	EA_ChatWindow.Print(towstring(L"GDes: "..str))
end

local function boolToString(bool)
	if bool then return "true" end
	return "false"
end
--Check if the GDes object already exists and if not create it
if (not GDes) then
	GDes = {}

	GDes.Comms = {}
	GDes.Comms.Channel = {}
	GDes.Comms.Channel.Cmd = ""
	GDes.Comms.Channel.Id  = nil
	GDes.Comms.Channel.LogName = ""
	GDes.Comms.Channel.Colour = { r = 128, g = 0, b = 128 }
	GDes.Comms.Channel.JoinAttempt = 0
	GDes.Comms.Channel.MaxAttempts = 50
	GDes.Comms.PlayerName = string.match(tostring(GameData.Player.name),"%a+")

	GDes.Debug = {}
	GDes.Debug.Enabled = false
	GDes.Debug.Level   = 2

	GDes.Button = {}
	GDes.Button.Window  = "GDesToggle"
	GDes.Button.Btn     = GDes.Button.Window.."Button"
	GDes.Button.Image   = GDes.Button.Window.."Image"
	GDes.Button.Hilight = GDes.Button.Window.."Hilight"

	GDes.Enabled      = true
	GDes.GuardCount   = 0
	GDes.Icons        = {}
	GDes.IconLayout   = "GDesIconLayoutEditor"
	GDes.Registered   = false
end

----------------------------
--- Core Addon Functions ---
----------------------------

-- This function is called whenever the addon is loaded.
function GDes.Init()
	GDes.DebugMsg(L"Entering Init",3)
	-- Check to see if any options are saved on the users profile.  If not set default values.
	if (not GDes.Settings) then
		GDes.Settings = {}
		GDes.Settings.Enabled = true
		GDes.Settings.ToggleColour = 6
		GDes.Settings.ShowButton = true

		GDes.Settings.Alerts = {}
		GDes.Settings.Alerts.Enabled = true
		GDes.Settings.Alerts.Channel = 8

		GDes.Settings.Icons = {}
		GDes.Settings.Icons.Enabled = true
		GDes.Settings.Icons.DefaultIcon   = 1
		GDes.Settings.Icons.DeadIcon      = 9
		GDes.Settings.Icons.isOfflineIcon = 7
		GDes.Settings.Icons.Extras = {
		   [GDesConstants.EXTRAS_TARGET_HEALTH] = { enabled = false , anchor = 5 , offsetX = 0 , offsetY = 0}
		 , [GDesConstants.EXTRAS_TARGET_NAME]   = { enabled = false , anchor = 1 , offsetX = 0 , offsetY = 0}
		 , [GDesConstants.EXTRAS_TARGET_CAREER] = { enabled = false , anchor = 3 , offsetX = 0 , offsetY = 0}
		                             }
		GDes.Settings.Icons.Health = {}
		GDes.Settings.Icons.Health.Enabled = true
		GDes.Settings.Icons.Health.FullIcon = 4
		GDes.Settings.Icons.Health.HighIcon = 3
		GDes.Settings.Icons.Health.MedIcon  = 2
		GDes.Settings.Icons.Health.MinIcon  = 5
		GDes.Settings.Icons.Health.HighPerc = 95
		GDes.Settings.Icons.Health.MedPerc  = 80
		GDes.Settings.Icons.Health.MinPerc  = 50
		GDes.Settings.Icons.MaxCount = GDesConstants.MAX_ICONS
		GDes.Settings.Icons.OffsetX  = 0
		GDes.Settings.Icons.OffsetY  = 0

		GDes.Settings.Sounds = {}
		GDes.Settings.Sounds.Enabled = false
		GDes.Settings.Sounds.Applied = 1
		GDes.Settings.Sounds.Removed = 1
		GDes.Settings.Version = 1.1
	end
	GDes.Settings.Version = VERSION
	-- Save a local version of enabled flag so that GDes can turn itself off if a none tank
    -- character is loaded, without the disable affecting the users profile settings.
	GDes.Enabled = GDes.Settings.Enabled
	-- Register the slash commands
	LibSlash.RegisterSlashCmd("gdes" , GDes.Slash)
	LibSlash.RegisterSlashCmd("gdesd", function(args) GDes.SlashDebug(args) end)
	-- If GDes isn't turned off in the settings then add event hooks
	if (GDes.Enabled) then
		GDes.RegisterEvents()
		printMsg(L"Is enabled. Type /gdes for options")
	else
		printMsg(L"Is disabled. Type /gdes for options")
	end
	-- Iniitalise the visual windows
	GDes.OptionsWindowInit()
	GDes.ToggleButtonInit()
	GDes.IconsInit()
	GDes.IconsLayoutInit()
	RegisterEventHandler(SystemData.Events.LOADING_END, "GDes.InitComplete")
	GDes.DebugMsg(L"Leaving Init",3)
end

function GDes.InitComplete()
	GDes.DebugMsg(L"Entering InitComplete",3)
	GDes.CommsFindChannel(true)
	-- Init complete, prevent this function from being called again.
	UnregisterEventHandler(SystemData.Events.LOADING_END, "GDes.InitComplete")
	GDes.DebugMsg(L"Leaving InitComplete",3)
end

function GDes.DebugMsg(str,lvl)
   -- If debug is enabled and the level of the message is high enough then print the string to the chat window
	if (GDes.Debug.Enabled and lvl <= GDes.Debug.Level) then
		d(towstring(str))
	end
end

function GDes.SlashDebug(args)
	GDes.DebugMsg(L"Entering SlashDebug",3)
	local arg, argv = args:match("([a-z0-9]+)[ ]?(.*)")
	GDes.DebugMsg("SlashDebug: arg ["..arg.."] argv ["..argv.."]",2)
	if arg == nil then
		GDes.Debug.Enabled = not GDes.Debug.Enabled
		return
	end
	-- Set to true to display debug messages in the debug window (Note: type /debug in game to see this window and press the button "Logs(off)")
	-- Set the debug level (4 = Update Ticks, 3 = Position, 2 = Setting values, 1 = Return values, 0 = Illegal flow catches)
	if arg == "on" then
		GDes.Debug.Enabled = true
	elseif arg == "off" then
		GDes.Debug.Enabled = false
	elseif arg == "1" then
		GDes.Debug.Enabled = true
		GDes.Debug.Level = 1
	elseif arg == "2" then
		GDes.Debug.Enabled = true
		GDes.Debug.Level = 2
	elseif arg == "3" then
		GDes.Debug.Enabled = true
		GDes.Debug.Level = 3
	elseif arg == "4" then
		GDes.Debug.Enabled = true
		GDes.Debug.Level = 4
	else
		GDes.DebugMsg("SlashDebug: Invalid Argument ["..arg.."]",0)
	end
	GDes.DebugMsg(L"Leaving SlashDebug",3)
end

function GDes.RegisterEvents()
	GDes.DebugMsg(L"Entering RegisterEvents",3)
	RegisterEventHandler(SystemData.Events.GROUP_UPDATED         , "GDes.UpdateTargetDetails")
	RegisterEventHandler(SystemData.Events.GROUP_STATUS_UPDATED  , "GDes.UpdateTargetDetails")
	RegisterEventHandler(SystemData.Events.BATTLEGROUP_UPDATED   , "GDes.UpdateTargetDetails")
	RegisterEventHandler(SystemData.Events.BATTLEGROUP_MEMBER_UPDATED, "GDes.UpdateTargetDetails")
	RegisterEventHandler(SystemData.Events.GROUP_LEAVE           , "GDes.UpdateTargetDetails")
	RegisterEventHandler(SystemData.Events.SCENARIO_GROUP_LEAVE  , "GDes.UpdateTargetDetails")
	RegisterEventHandler(SystemData.Events.CHAT_TEXT_ARRIVED     , "GDes.CommsReceived")
	GDes.Registered = true
	GDes.DebugMsg(L"Leaving RegisterEvents",3)
end

function GDes.UnRegisterEvents()
	GDes.DebugMsg(L"Entering UnRegisterEvents",3)
	if GDes.Registered then
		UnregisterEventHandler(SystemData.Events.GROUP_UPDATED        , "GDes.UpdateTargetDetails")
		UnregisterEventHandler(SystemData.Events.GROUP_STATUS_UPDATED , "GDes.UpdateTargetDetails")
		UnregisterEventHandler(SystemData.Events.BATTLEGROUP_UPDATED  , "GDes.UpdateTargetDetails")
		UnregisterEventHandler(SystemData.Events.BATTLEGROUP_MEMBER_UPDATED, "GDes.UpdateTargetDetails")
		UnregisterEventHandler(SystemData.Events.GROUP_LEAVE          , "GDes.UpdateTargetDetails")
		UnregisterEventHandler(SystemData.Events.SCENARIO_GROUP_LEAVE , "GDes.UpdateTargetDetails")
		UnregisterEventHandler(SystemData.Events.CHAT_TEXT_ARRIVED    , "GDes.CommsReceived")
		GDes.Registered = false
	end
	GDes.DebugMsg(L"Leaving UnRegisterEvents",3)
end

function GDes.Shutdown()
	GDes.DebugMsg(L"Entering Shutdown",3)
	-- Mod is shutdown, unsubscribe from events.
	GDes.IconsReset()
	GDes.UnRegisterEvents()
	GDes.Enabled = false
	GDes.DebugMsg(L"Leaving Shutdown",3)
end

function GDes.Slash()
	GDes.DebugMsg(L"Entering Slash",3)
	-- When user types /ges show the Options window.
	GDes.OptionsShow()
	GDes.DebugMsg(L"Leaving Slash",3)
end

-- Function is called when a group member is updated
function GDes.UpdateTargetDetails(groupMemberIndex)
	GDes.DebugMsg(L"Entering UpdateTargetDetails",3)
	local guardFound = 0
	if not GDes.Enabled or GDes.GuardCount == 0 then return end
	GDes.DebugMsg(L"UTD: Getting all party details",3)
	-- Looping through Party Details and pair them with Icons
	for k,a in pairs (PartyUtils.GetPartyData()) do
		if a.name ~= L"" then
			if GDes.IconsUpdate(a) then
				guardFound = guardFound + 1
			end
		end
	end
	-- Check to see the number of guards updated equals the number of active icons
	if guardFound < GDes.GuardCount then
		-- If we're here we have more active icons than there should be, time to validate them
		GDes.IconsValidate()
	end

	GDes.DebugMsg(L"Leaving UpdateTargetDetails",3)
end

-------------------------------
---     Icons Functions     ---
-------------------------------
function GDes.IconsInit()
	GDes.DebugMsg(L"Entering IconsInit",3)
	for k = 1, GDes.Settings.Icons.MaxCount do
		GDes.Icons[k] = GDesIcon:new(k)
		GDes.Icons[k]:Init()
	end
	GDes.Icons[GDesConstants.ICON_REFKEY] = {}
	GDes.DebugMsg(L"Leaving IconsInit",3)
end

function GDes.IconsReset()
	GDes.DebugMsg(L"Entering IconsReset",3)
	for k, a in pairs(GDes.Icons) do
		if k ~= GDesConstants.ICON_REFKEY then
			a:Deactivate()
		end
	end
	GDes.Icons[GDesConstants.ICON_REFKEY] = {}
	GDes.GuardCount = 0
	GDes.DebugMsg(L"Leaving IconsReset",3)
end

function GDes.IconsAssign(newName)
	GDes.DebugMsg(L"Entering AssignIcon",3)
	-- Check to ensure we don't already have an active icon for this guard
	local iconKey = GDes.Icons[GDesConstants.ICON_REFKEY][tostring(newName)]
	if iconKey == nil then
		-- Valid new entry so process accordingly
		for k, a in pairs(GDes.Icons) do
			if k ~= GDesConstants.ICON_REFKEY and not a:GetActive() then
				a:Activate(tostring(newName))
				GDes.Icons[GDesConstants.ICON_REFKEY][tostring(newName)] = k
				GDes.DebugMsg("AssignIcon: Icon ["..k.."] has been assigned",2)
				GDes.DebugMsg(L"Leaving AssignIcon",3)
				GDes.GuardCount = GDes.GuardCount + 1
				return true
			end
		end
		if GDes.IconsActiveCount() == GDes.Settings.Icons.MaxCount then
			GDes.DebugMsg(L"AssignIcon: Failed to assign as the max number of Guard icons has already been reached",2)
		else
			GDes.DebugMsg(L"AssignIcon: Failed to assign an icon for no obvious reason",0)
		end
	else
		GDes.DebugMsg(L"AssignIcon: Attempted to assign an icon to a guard that already has an icon!!",0)
		GDes.DebugMsg("AssignIcon: Name ["..tostring(newName).."] EntityId ["..tostring(newEntityId).."]",2)
	end
	GDes.DebugMsg(L"Leaving AssignIcon",3)
	return false
end

function GDes.IconsRelease(name)
	GDes.DebugMsg(L"Entering IconsRelease",3)
	local iconKey = GDes.Icons[GDesConstants.ICON_REFKEY][tostring(name)]
	if iconKey ~= nil then
		GDes.Icons[iconKey]:Deactivate()
		GDes.GuardCount = GDes.GuardCount - 1
		GDes.Icons[GDesConstants.ICON_REFKEY][tostring(name)] = nil
		GDes.DebugMsg("IconsRelease: Icon ["..iconKey.."] has been released",2)
	end
	GDes.DebugMsg(L"Leaving IconsRelease",3)
end

function GDes.IconsUpdate(newDetails)
	GDes.DebugMsg(L"Entering IconsUpdate",3)
	local found = false
	if newDetails == nil then return found end
	--local name = string.match(tostring(newDetails.name),"%a+")
	GDes.DebugMsg("IconsUpdate: Name ["..tostring(newDetails.name).."]",2)
	local iconKey = GDes.Icons[GDesConstants.ICON_REFKEY][tostring(newDetails.name)]
	if iconKey ~= nil then
		GDes.DebugMsg("IconsUpdate: iconKey ["..iconKey.."]",2)
		found = GDes.Icons[iconKey]:UpdateTarget(newDetails)
		GDes.DebugMsg("IconsUpdate: Icon ["..iconKey.."] has been updated",2)
	end
	GDes.DebugMsg(L"Leaving IconsUpdate",3)
	return found
end

function GDes.IconsValidate()
	GDes.DebugMsg(L"Entering IconsValidate",3)
	local partyDetails = {}
	-- Create a single table with the party member names as the index
	for k, a in pairs (PartyUtils.GetPartyData()) do
		partyDetails[tostring(a.name)] = a.online
	end
	-- Loop through the icons checking the icon targets name against party member names to check if icon is still valid
	for k, a in pairs(GDes.Icons) do
		if k ~= GDesConstants.ICON_REFKEY and a:GetActive() then
			if not partyDetails[a:GetTargetName()] then
				GDes.IconsRelease(a:GetTargetName())
			end
		end
	end
	GDes.DebugMsg(L"Leaving IconsValidate",3)
end

function GDes.IconsUpdateMaxCount()
	GDes.DebugMsg(L"Entering IconsUpdateMaxCount",3)
	local curIconCount = (#GDes.Icons -1)
	local diff = curIconCount - GDes.Settings.Icons.MaxCount
	-- Check to see if there had been a change, if not then exit
	if GDes.Settings.Icons.MaxCount == curIconCount then GDes.DebugMsg(L"Icons already at MaxCount",3) return end
	-- Check to see if the count has increased or decrease and act accordingly
	if diff < 0 then
		GDes.IconsIncreaseCount(diff * -1)
	else
		GDes.IconsReduceCount(diff, GDes.Settings.Icons.MaxCount >= GDes.GuardCount)
	end
	GDes.DebugMsg(L"Leaving IconsUpdateMaxCount",3)
end

function GDes.IconsReduceCount(amount, checkActive)
	GDes.DebugMsg(L"Entering IconsReduceCount",3)
	local release = false
	-- Cycle through the table of icons and remove the required amount
	for k, a in pairs(GDes.Icons) do
		-- Check to ensure we don't remove the Reference table by mistake.
		if k ~= GDesConstants.ICON_REFKEY and amount > 0 then
			release = true
			-- Check to see if the icon is active and whether we care
			if checkActive and a:GetActive() then
				release = false
			end
			if release then
				if a:GetActive() then
					-- Icon to be removed is currently active, to adjust the reference data accordingly
					GDes.GuardCount = GDes.GuardCount - 1
					table.remove(GDes.Icons[GDesConstants.ICON_REFKEY],a:GetTargetName())
				end
				-- Icon to be removed needs to shutdown to destroy it's registered windows etc.
				a:Shutdown()
				table.remove(GDes.Icons,k)
				GDes.DebugMsg("IconsReduceCount: Icon ["..k.."] has been removed",2)
				amount = amount - 1
			end
		end
	end
	GDes.DebugMsg(L"Leaving IconsReduceCount",3)
end

function GDes.IconsIncreaseCount(amount)
	GDes.DebugMsg(L"Entering IconsIncreaseCount",3)
	for k = 1, GDes.Settings.Icons.MaxCount do
		if GDes.Icons[k] == nil and amount > 0 then
			GDes.Icons[k] = GDesIcon:new(k)
			GDes.Icons[k]:Init()
			amount = amount - 1
		end
	end
	GDes.DebugMsg(L"Leaving IconsIncreaseCount",3)
end

function GDes.IconsLayoutInit()
	GDes.DebugMsg(L"Entering IconsLayoutInit",3)
	-- Create a layout version of the tracker icon so the Layout editor doesn't interfere with the AttachWindowToObject function
	CreateWindow(GDes.IconLayout, true)
	WindowSetShowing(GDes.IconLayout,false)
	local iconTemplate
	for k,a in pairs(GDes.Icons) do
		if k ~= GDesConstants.ICON_REFKEY then
			iconTemplate = a
			-- We only need to do this with the first Icon we find.
			break
		end
	end
	-- Set the size of the layout version to be the same as an actual Icon window
	WindowSetDimensions(GDes.IconLayout, iconTemplate:GetWindowDimensions())
	WindowSetScale     (GDes.IconLayout, iconTemplate:GetWindowScale())
	WindowSetAlpha     (GDes.IconLayout, iconTemplate:GetWindowAlpha())
	-- Anchor the layout version to the actual icon so that it appear above it in the layout editor
	WindowClearAnchors(GDes.IconLayout)
	WindowAddAnchor(GDes.IconLayout, "center", "Root", "center", -300, 0)
	-- Register window with the layout editor so that it can be moved and resized there.
	LayoutEditor.RegisterWindow ( GDes.IconLayout   -- windowName
                               , L"GDes Icon"      -- windowDisplayName
	                            , L"The GDes icon"  -- windowDesc
                               , false             -- allowSizeWidth
                               , false             -- allowSizeHeight
                               , true              -- allowHiding
                               , nil               -- allowableAnchorList
                               )
	LayoutEditor.RegisterEditCallback(GDes.IconsLayoutUpdate)
	GDes.DebugMsg(L"Leaving IconsLayoutInit",3)
end

function GDes.IconsLayoutUpdate(status)
	GDes.DebugMsg(L"Entering IconsLayoutUpdate",3)
	if status == LayoutEditor.EDITING_END then
		-- Loop through the icons resize them to be the same as the Layout version.
		for k,a in pairs(GDes.Icons) do
			if k ~= GDesConstants.ICON_REFKEY then
				local isShowing = a:GetWindowShowing()
				a:SetWindowShowing(false)
				a:SetWindowAlpha     (WindowGetAlpha     (GDes.IconLayout))
				a:SetWindowDimensions(WindowGetDimensions(GDes.IconLayout))
				a:SetWindowScale     (WindowGetScale     (GDes.IconLayout))
				a:SetWindowShowing(isShowing)
			end
		end
	end
	GDes.DebugMsg(L"Leaving IconsLayoutUpdate",3)
end

-----------------------------------
-- Inter Communication functions --
-----------------------------------
function GDes.CommsFindChannel(reset)
	-- Check to see if the channel has already been found
	if GDes.Comms.Channel.Cmd == "" or reset then
		-- Locate which channel we have been assigned to
		for k, a in pairs(ChatSettings.Channels) do
			if a ~= nil and a.id ~= nil and a.serverCmd ~= nil and a.labelText ~= nil and a.logName ~= nil then
				GDes.DebugMsg(L"CFC: Id ["..a.id..L"] ServerCmd ["..a.serverCmd..L"] LogName ["..towstring(a.logName)..L"] LabelText ["..a.labelText..L"]", 2)
				local label = tostring(a.labelText)
				if label:match(GDesConstants.Comms.Channels[GameData.Player.realm]) then
					GDes.Comms.Channel.Cmd = tostring(a.serverCmd)
					GDes.Comms.Channel.Id = a.id
					GDes.Comms.Channel.LogName = a.logName
					ChatSettings.ChannelColors[GDes.Comms.Channel.Id] = GDes.Comms.Channel.Colour
					GDes.CommsHideChannel()
					GDes.DebugMsg(L"Leaving CommsFindChannel - Channel found",3)
					return GDes.Comms.Channel.Cmd
				end
			end
		end
		-- Chennel was not found, so join it
		GDes.DebugMsg(L"CommsFindChannel: Channel not found", 0)
		GDes.CommsJoinChannel()
		return nil
	end
	return GDes.Comms.Channel.Cmd
end

function GDes.CommsJoinChannel()
	GDes.DebugMsg(L"Entering CommsJoinChannel",3)
	-- if GDes.Comms.Channel.JoinAttempt > GDes.Comms.Channel.MaxAttempts then
		-- The addon has tried to join the channel more than GDes.Comms.Channel.MaxAttempts times, something is wrong, so shut down and stop draining resources.
		-- EA_ChatWindow.Print(L"GDes: Unable to join channel ["..towstring(GDesConstants.Comms.Channels[GameData.Player.realm])..L"] after "..towstring(GDes.Comms.Channel.MaxAttempts)..L" attempts. Shutting down.")
		-- GDes.Shutdown()
	-- end
	GDes.Comms.Channel.JoinAttempt = GDes.Comms.Channel.JoinAttempt + 1
	SendChatText(L"/channeljoin "..towstring(GDesConstants.Comms.Channels[GameData.Player.realm]),L"")
	GDes.DebugMsg(L"Leaving CommsJoinChannel",3)
end

function GDes.CommsHideChannel()
	GDes.DebugMsg(L"Entering CommsHideChannel",3)
	-- Auto filter out the comms channel from all other tabs aside from the testing tab.
	for k, a in pairs(EA_ChatWindowGroups) do
		if a ~= nil and a.Tabs ~= nil then
			for g, e in pairs(a.Tabs) do
				local windowName = EA_ChatTabManager.GetTabName(e.tabManagerId).."TextLog"
				if e.tabText == L"Kage" then
					LogDisplaySetFilterState(windowName, GDes.Comms.Channel.LogName, GDes.Comms.Channel.Id, true)
					LogDisplaySetFilterColor(windowName, GDes.Comms.Channel.LogName, GDes.Comms.Channel.Id, GDes.Comms.Channel.Colour.r, GDes.Comms.Channel.Colour.g, GDes.Comms.Channel.Colour.b)
				else
					LogDisplaySetFilterState(windowName, GDes.Comms.Channel.LogName, GDes.Comms.Channel.Id, false)
				end
				-- Hide the Join/Leave channel messages.
				LogDisplaySetFilterState(windowName, "Chat", 6, false)
			end
		end
	end
	LogDisplaySetFilterState( "SiegeWeaponGeneralFireWindowChatLogDisplay", GDes.Comms.Channel.LogName, GDes.Comms.Channel.Id, false )
	GDes.DebugMsg(L"Leaving CommsHideChannel",3)
end

function GDes.CommsReceived()
	--GDes.DebugMsg("CR: Type["..tostring(GameData.ChatData.type).."] Name["..tostring(GameData.ChatData.name).."]", 3)
	-- Check to see if the Kage channel has been found, if not find it.
	if GDes.CommsFindChannel(false) == nil then return end
	-- Check to see if the chat is coming from the Kage channel, if not exit
	if GDes.Comms.Channel.Id ~= GameData.ChatData.type then return end
	GDes.DebugMsg(L"Entering CommsReceived",3)
	-- Retrieve the chat contents
	local contents = StringSplit(tostring(GameData.ChatData.text),"|")
	-- Check to see if the contents are specific to the Ges addon
	if contents[GDesConstants.COMMS_ADDON] ~= "Ges" then GDes.DebugMsg(L"CommsReceived: Not a Ges comms", 3) return end

	GDes.DebugMsg("Msg Text: Addon ["..contents[GDesConstants.COMMS_ADDON].."]",2)
	GDes.DebugMsg("Msg Text: Area ["..contents[GDesConstants.COMMS_AREA_ID].."]",2)
	GDes.DebugMsg("Msg Text: Type ["..contents[GDesConstants.COMMS_STATUS].."]",2)
	GDes.DebugMsg("Msg Text: CasterID ["..contents[GDesConstants.COMMS_CASTER_ID].."]",2)
	GDes.DebugMsg("Msg Text: CasrerName ["..contents[GDesConstants.COMMS_CASTER_NAME].."]",2)
	GDes.DebugMsg("Msg Text: TargetId ["..contents[GDesConstants.COMMS_TARGET_ID].."]",2)
	GDes.DebugMsg("Msg Text: TargetName ["..contents[GDesConstants.COMMS_TARGET_NAME].."]",2)

	-- Check to see if the comms applies to the player
	if contents[GDesConstants.COMMS_TARGET_NAME] == GDes.Comms.PlayerName then
		GDes.CommsProcess( tonumber(contents[GDesConstants.COMMS_STATUS]) == GDesConstants.CAST_SUCCESS
		                -- , string.match(contents[GDesConstants.COMMS_CASTER_NAME],"%a+")
						 , contents[GDesConstants.COMMS_CASTER_NAME]
		                 )
	else
		GDes.DebugMsg(L"CommsReceived: Guard not for this player", 3)
	end
	GDes.DebugMsg(L"Leaving CommsReceived",3)
end

function GDes.CommsProcess(Activate, GuardName)
	GDes.DebugMsg(L"Entering CommsProcess",3)
	GDes.DebugMsg("CP: Activate ["..boolToString(Activate).."] GuardName ["..GuardName.."]",3)
	if Activate then
		if GDes.IconsAssign(GuardName) then
			GDes.UpdateTargetDetails()
		end
	else
		GDes.IconsRelease(GuardName)
	end
	GDes.CommsAlert(Activate, GuardName)
	GDes.CommsSound(Activate)
	GDes.DebugMsg(L"Leaving CommsProcess",3)
end

function GDes.CommsAlert(Activate, GuardName)
	GDes.DebugMsg(L"Entering CommsAlert",3)
	if not GDes.Settings.Alerts.Enabled then return end
	local message
	if Activate then
		message = GuardName.." is now guarding you"
	else
		message = GuardName.." stopped guarding you"
	end
	AlertTextWindow.AddLine(GDesConstants.Alerts[GDes.Settings.Alerts.Channel].id, towstring(message))
	GDes.DebugMsg(L"Leaving CommsAlert",3)
end

function GDes.CommsSound(Activate)
	GDes.DebugMsg(L"Entering CommsAlert",3)
	if not GDes.Settings.Sounds.Enabled then return end
    local selection
	if Activate then
		selection = GDes.Settings.Sounds.Applied
	else
       selection = GDes.Settings.Sounds.Removed
	end
	if selection > 1 then
		Sound.Play(GDesConstants.SoundTypes[selection].id)
	end
	GDes.DebugMsg(L"Leaving CommsAlert",3)
end
---------------------------------------------
---        UI Specific Functions          ---
---------------------------------------------
-------------------------------
--- Toggle Button Functions ---
-------------------------------
function GDes.ToggleButtonInit()
	GDes.DebugMsg(L"Entering ToggleButtonInit",3)
	-- Create and register the toggle button window and then show/hide it based on the settings.
	CreateWindow(GDes.Button.Window, true)
	-- This is not a typo, for some reason the ButtonSetTextures function only works if
	-- the window is first hidden and then shown, no idea why and it was a pain to debug.
	WindowSetShowing(GDes.Button.Window,false)
	WindowSetShowing(GDes.Button.Window,GDes.Settings.ShowButton)
	-- Register window with the layout editor so that it can be moved and GDesized there.
	LayoutEditor.RegisterWindow( GDes.Button.Window             -- windowName
                               , L"GDes Button"                 -- windowDisplayName
                               , L"The toggle button for GDes"  -- windowDesc
                               , false                          -- allowSizeWidth
                               , false                          -- allowSizeHeight
                               , true                           -- allowHiding
                               , nil                            -- allowableAnchorList
                               )
	GDes.ToggleButtonSetTextures()
	GDes.DebugMsg(L"Leaving ToggleButtonInit",3)
end

function GDes.ToggleButtonSetTextures()
	GDes.DebugMsg(L"Entering ToggleButtonSetTextures",3)
	-- Set the background texture and slice for the Toggle Button
	DynamicImageSetTexture( GDes.Button.Image
	                      , GDesConstants.IconsBackgrounds[GDes.Settings.ToggleColour].texture
	                      , 0, 0
	                      )
	DynamicImageSetTextureSlice( GDes.Button.Image
	                           , GDesConstants.IconsBackgrounds[GDes.Settings.ToggleColour].slice
	                           )
	-- Set the foreground texture and slice for the Toggle Button for both normal and highlighted conditions.
	ButtonSetTextureSlice( GDes.Button.Btn
	                     , Button.ButtonState.NORMAL
	                     , GDesConstants.IconsForegrounds[GDesConstants.FOREGROUND_TEXTBG].texture
	                     , GDesConstants.IconsForegrounds[GDesConstants.FOREGROUND_TEXTBG].slice
	                     )
	-- Set the foreground texture and slice for the Toggle Button Icon
	DynamicImageSetTexture( GDes.Button.Hilight
	                      , GDesConstants.IconsForegrounds[GDesConstants.FOREGROUND_HILIGHT].texture
	                      , 0, 0
	                      )
	DynamicImageSetTextureSlice( GDes.Button.Hilight
	                           , GDesConstants.IconsForegrounds[GDesConstants.FOREGROUND_HILIGHT].slice
	                           )
	GDes.DebugMsg(L"Leaving ToggleButtonSetTextures",3)
end

function GDes.Toggle(flags, mouseX, mouseY)
	GDes.DebugMsg(L"Entering Toggle",3)
	-- Check the status of GDes and alter it to the opposite
	if (GDes.Enabled) then
		GDes.UnRegisterEvents()
		GDes.Settings.Enabled = false
		GDes.Enabled = false
	else
		GDes.RegisterEvents()
		GDes.Settings.Enabled = true
		GDes.Enabled = true
	end
	GDes.IconsReset()
	GDes.ToggleButtonMouseOver()
	GDes.DebugMsg(L"Leaving Toggle",3)
end

function GDes.ToggleButtonMouseOver(flags, mouseX, mouseY)
	GDes.DebugMsg(L"Entering ToggleButtonMouseOver",3)
	local TextStatus  = L""
	local TextToggle  = L""

	-- Generate the tooltip message
	if (GDes.Enabled) then
		 TextStatus = L"Enabled"
		 TextToggle = L"Disable"
	else
		 TextStatus = L"Disabled"
		 TextToggle = L"Enable"
	end

	local msg = L"\nLeft Click to "..TextToggle..L" GDes"
	msg = msg..L"\nRight Click to view the Options Window\n"
	-- Set the tooltip message
	Tooltips.CreateTextOnlyTooltip( SystemData.ActiveWindow.name, nil)
	Tooltips.SetTooltipText (1, 1, L"GDes is currently "..TextStatus)
	if (GDes.Enabled) then
		Tooltips.SetTooltipColor(1, 1, 0, 255, 0)
	else
		Tooltips.SetTooltipColor(1, 1, 255, 0, 0)
	end
	Tooltips.SetTooltipText (2, 1, towstring(msg))
	Tooltips.AnchorTooltip(Tooltips.ANCHOR_WINDOW_BOTTOM)
	Tooltips.Finalize()
	GDes.DebugMsg(L"Leaving ToggleButtonMouseOver",3)
end

