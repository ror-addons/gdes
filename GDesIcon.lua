GDesIcon = {}
GDesIconMetaTable = {}
GDesIconMetaTable.__index = GDesIcon -- redirect queries to the GDesIcon table

local function boolToString(bool)
	if bool then return "true" end
	return "false"
end

function GDesIcon:new(id)
	-- Create and initialise all of the member variables
	newIcon = {}
	newIcon.IconId = id
	newIcon.Active = false
	newIcon.Window = "GDesIcon"..id
	newIcon.Anchor = newIcon.Window.."Anchor"
	newIcon.AttachEntityId = 0
	newIcon.Extras = {
		  [GDesConstants.EXTRAS_TARGET_HEALTH] = { name = newIcon.Window.."TargetHealth" , enabled = false , anchorId = 0 , offsetX = 0 , offsetY = 0}
		, [GDesConstants.EXTRAS_TARGET_NAME]   = { name = newIcon.Window.."TargetName"   , enabled = false , anchorId = 0 , offsetX = 0 , offsetY = 0}
		, [GDesConstants.EXTRAS_TARGET_CAREER] = { name = newIcon.Window.."CareerIcon"   , enabled = false , anchorId = 0 , offsetX = 0 , offsetY = 0}
	                 }
	newIcon.Hilight = {}
	newIcon.Hilight.Name = newIcon.Window.."Hilight"
	newIcon.Hilight.Selection = 1
	newIcon.Image = {}
	newIcon.Image.Name = newIcon.Window.."Image"
	newIcon.Image.Selection = 1
	newIcon.isAttached = false
	newIcon.Status = {}
	newIcon.Status.Name = newIcon.Window.."Status"
	newIcon.Status.Selection = 1
	newIcon.Target = {}
	newIcon.Target.Career = {}
	newIcon.Target.Career.Line    = 0
	newIcon.Target.Career.Texture = 0
	newIcon.Target.Career.OffsetX = 0
	newIcon.Target.Career.OffsetY = 0
	newIcon.Target.EntityId       = 0
	newIcon.Target.EntityIdOld    = 0
	newIcon.Target.HealthPerc     = 0
	newIcon.Target.isDistant      = false
	newIcon.Target.isDead         = false
	newIcon.Target.isOnline       = false
	newIcon.Target.isInRegion     = false
	newIcon.Target.Name           = ""
	-- Allocate the member variables to their own distinct MetaTable.
    return setmetatable({ mv = newIcon}, GDesIconMetaTable)
end

-----------------------------
---     Get Functions     ---
-----------------------------
-- Define all of the get functions for our member variables.
function GDesIcon:GetActive()              return (self.mv.Active)                end
function GDesIcon:GetAnchor()              return (self.mv.Anchor)                end
function GDesIcon:GetAttachEntityId()      return (self.mv.AttachEntityId)        end
function GDesIcon:GetWindow()              return (self.mv.Window)                end
function GDesIcon:GetHilightName()         return (self.mv.Hilight.Name)          end
function GDesIcon:GetHilightSelection()    return (self.mv.Hilight.Selection)     end
function GDesIcon:GetIconId()              return (self.mv.IconId)                end
function GDesIcon:GetImageName()           return (self.mv.Image.Name)            end
function GDesIcon:GetImageSelection()      return (self.mv.Image.Selection)       end
function GDesIcon:GetIsAttached()          return (self.mv.isAttached)            end
function GDesIcon:GetExtrasName(id)        return (self.mv.Extras[id].name)       end
function GDesIcon:GetExtrasEnabled(id)     return (self.mv.Extras[id].enabled)    end
function GDesIcon:GetExtrasAnchorId(id)    return (self.mv.Extras[id].anchorId)   end
function GDesIcon:GetExtrasOffsetX(id)     return (self.mv.Extras[id].offsetX)    end
function GDesIcon:GetExtrasOffsetY(id)     return (self.mv.Extras[id].offsetY)    end
function GDesIcon:GetStatusName()          return (self.mv.Status.Name)           end
function GDesIcon:GetStatusSelection()     return (self.mv.Status.Selection)      end
function GDesIcon:GetTargetCareerLine()    return (self.mv.Target.Career.Line)    end
function GDesIcon:GetTargetCareerTexture() return (self.mv.Target.Career.Texture) end
function GDesIcon:GetTargetCareerOffsetX() return (self.mv.Target.Career.OffsetX) end
function GDesIcon:GetTargetCareerOffsetY() return (self.mv.Target.Career.OffsetY) end
function GDesIcon:GetTargetEntityId()      return (self.mv.Target.EntityId)       end
function GDesIcon:GetTargetEntityIdOld()   return (self.mv.Target.EntityIdOld)    end
function GDesIcon:GetTargetHealthPerc()    return (self.mv.Target.HealthPerc)     end
function GDesIcon:GetTargetisDistant()     return (self.mv.Target.isDistant)      end
function GDesIcon:GetTargetisDead()        return (self.mv.Target.isDead)         end
function GDesIcon:GetTargetisOnline()      return (self.mv.Target.isOnline)       end
function GDesIcon:GetTargetisInRegion()    return (self.mv.Target.isInSameRegion) end
function GDesIcon:GetTargetName()          return (self.mv.Target.Name)           end
function GDesIcon:GetWindowAlpha()         return WindowGetAlpha(self:GetWindow())      end
function GDesIcon:GetWindowDimensions()    return WindowGetDimensions(self:GetWindow()) end
function GDesIcon:GetWindowScale()         return WindowGetScale(self:GetWindow())      end
function GDesIcon:GetWindowShowing()       return WindowGetShowing(self:GetWindow())    end

-----------------------------
---     Set Functions     ---
-----------------------------
-- Define all of the set functions for our member variables.
function GDesIcon:SetActive(newValue)                self.mv.Active = newValue                  end
function GDesIcon:SetAnchor(newValue)                self.mv.Anchor = newValue                  end
function GDesIcon:SetAttachEntityId(newEntityId)     self.mv.AttachEntityId = newEntityId       end
function GDesIcon:SetWindow(newValue)                self.mv.Window = newValue                  end
function GDesIcon:SetHilightName(newName)            self.mv.Hilight.Name = newName             end
function GDesIcon:SetHilightSelection(newSelection)  self.mv.Hilight.Selection = newSelection   end
function GDesIcon:SetImageName(newName)              self.mv.Image.Name = newName               end
function GDesIcon:SetImageSelection(newSelection)    self.mv.Image.Selection = newSelection     end
function GDesIcon:SetIsAttached(newValue)            self.mv.isAttached = newValue              end
function GDesIcon:SetExtrasName(id, newName)         self.mv.Extras[id].name = newName          end
function GDesIcon:SetExtrasEnabled(id, newEnabled)   self.mv.Extras[id].enabled = newEnabled    end
function GDesIcon:SetExtrasAnchorId(id, newAnchorId) self.mv.Extras[id].anchorId = newAnchorId  end
function GDesIcon:SetExtrasOffsetX(id, newOffsetX)   self.mv.Extras[id].offsetX = newOffsetX    end
function GDesIcon:SetExtrasOffsetY(id, newOffsetY)   self.mv.Extras[id].offsetY = newOffsetY    end
function GDesIcon:SetStatusName(newName)             self.mv.Status.Name = newName              end
function GDesIcon:SetStatusSelection(newSelection)   self.mv.Status.Selection = newSelection    end
function GDesIcon:SetTargetCareerLine(newCareerLine) self.mv.Target.Career.Line = newCareerLine end
function GDesIcon:SetTargetCareerTexture(newTexture) self.mv.Target.Career.Texture = newTexture end
function GDesIcon:SetTargetCareerOffsetX(newOffsetX) self.mv.Target.Career.OffsetX = newOffsetX end
function GDesIcon:SetTargetCareerOffsetY(newOffsetY) self.mv.Target.Career.OffsetY = newOffsetY end
function GDesIcon:SetTargetEntityId(newEntityId)     self.mv.Target.EntityId = newEntityId      end
function GDesIcon:SetTargetEntityIdOld(newEntityId)  self.mv.Target.EntityIdOld = newEntityId   end
function GDesIcon:SetTargetHealthPerc(newHealth)     self.mv.Target.HealthPerc = tonumber(tostring(newHealth)) end
function GDesIcon:SetTargetisDistant(newDistant)     self.mv.Target.isDistant = newDistant      end
function GDesIcon:SetTargetisDead(newisDead)         self.mv.Target.isDead = newisDead          end
function GDesIcon:SetTargetisOnline(newisOnline)     self.mv.Target.isOnline = newisOnline      end
function GDesIcon:SetTargetisInRegion(newisInRegion) self.mv.Target.isInRegion = newisInRegion  end
function GDesIcon:SetTargetName(newName)             self.mv.Target.Name = tostring(newName)    end
function GDesIcon:SetWindowAlpha(newAlpha)           WindowSetAlpha(self:GetWindow(), newAlpha)        end
function GDesIcon:SetWindowDimensions(newX, newY)    WindowSetDimensions(self:GetWindow(), newX, newY) end
function GDesIcon:SetWindowScale(newScale)           WindowSetScale(self:GetWindow(), newScale)        end
function GDesIcon:SetWindowShowing(show)             WindowSetShowing(self:GetWindow(), show)          end

---------------------------------------
---  Start Functional....Functions  ---
---------------------------------------
function GDesIcon:Init()
	GDes.DebugMsg("Entering Icon ["..self:GetIconId().."]:Init",3)
	-- Create and hide the anchor window
	CreateWindow(self:GetAnchor(), true)
	WindowSetShowing(self:GetAnchor(), true)
	-- Create and hide the icon window
	CreateWindow(self:GetWindow(), true)
	WindowSetShowing(self:GetWindow(), false)
	WindowSetParent (self:GetWindow(), self:GetAnchor())
	-- Cycle through the optional extras and create a local version specific to the icon
	for i,v in ipairs(GDes.Settings.Icons.Extras) do
		self:SetExtrasEnabled  (i, v.enabled )
		self:SetExtrasAnchorId (i, v.anchor  )
		self:SetExtrasOffsetX  (i, v.offsetX )
		self:SetExtrasOffsetY  (i, v.offsetY )
		self:SetExtrasAnchor   (i)
	end
	-- Set the Icon anchor to the saved value
	self:SetAnchor(GDes.Settings.Icons.OffsetX, GDes.Settings.Icons.OffsetY)
	GDes.DebugMsg("Leaving Icon ["..self:GetIconId().."]:Init",3)
end

function GDesIcon:SetAnchor(OffsetX, OffsetY)
	GDes.DebugMsg("Entering Icon ["..self:GetIconId().."]:SetAnchor",3)
	WindowClearAnchors(self:GetWindow())
	WindowAddAnchor( self:GetWindow()
	               , "center"
	               , self:GetAnchor()
	               , "center"
	               , tonumber(OffsetX)
	               , tonumber(OffsetY)
	               )
	GDes.DebugMsg("Leaving Icon ["..self:GetIconId().."]:SetAnchor",3)
end

function GDesIcon:SetExtrasAnchor(ExtraId)
	GDes.DebugMsg("Entering SetExtrasAnchor",3)
	WindowClearAnchors(self:GetExtrasName(ExtraId))
	WindowAddAnchor( self:GetExtrasName(ExtraId)
	               , GDesConstants.Anchors[self:GetExtrasAnchorId(ExtraId)].name
	               , self:GetWindow()
	               , GDesConstants.Anchors[self:GetExtrasAnchorId(ExtraId)].opposite
	               , tonumber(GDesConstants.Anchors[self:GetExtrasAnchorId(ExtraId)].offsetX + self:GetExtrasOffsetX(ExtraId))
	               , tonumber(GDesConstants.Anchors[self:GetExtrasAnchorId(ExtraId)].offsetY + self:GetExtrasOffsetY(ExtraId))
	               )
	GDes.DebugMsg("Leaving SetExtrasAnchor",3)
end

function GDesIcon:Shutdown()
	GDes.DebugMsg("Entering Icon ["..self:GetIconId().."]:Shutdown",3)
	self:Deactivate()
	DestroyWindow(self:GetAnchor())
	DestroyWindow(self:GetWindow())
	GDes.DebugMsg("Leaving Icon ["..self:GetIconId().."]:Shutdown",3)
end

function GDesIcon:Activate(name)
	GDes.DebugMsg("Entering Icon ["..self:GetIconId().."]:Activate",3)
	-- Show the window and attach the Anchor to the target
	self:SetTargetName(name)
	self:SetTargetisOnline(true)
	self:SetActive(true)
	GDes.DebugMsg("Leaving Icon ["..self:GetIconId().."]:Activate",3)
end

function GDesIcon:Deactivate()
	GDes.DebugMsg("Entering Icon ["..self:GetIconId().."]:Deactivate",3)
	-- Detach the icon from the target and reset all of the icons values back to default.
	self:Detach()
	self:SetAttachEntityId(0)
	self:SetHilightSelection(1)
	self:SetImageSelection(1)
	self:SetStatusSelection(1)
	self:SetTargetCareerLine(0)
	self:SetTargetCareerTexture(0)
	self:SetTargetCareerOffsetX(0)
	self:SetTargetCareerOffsetY(0)
	self:SetTargetEntityId(0)
	self:SetTargetEntityIdOld(0)
	self:SetTargetHealthPerc(0)
	self:SetTargetisDistant(false)
	self:SetTargetisDead(false)
	self:SetTargetisOnline(false)
	self:SetTargetisInRegion(false)
	self:SetTargetName("")
	self:SetActive(false)
	GDes.DebugMsg("Leaving Icon ["..self:GetIconId().."]:Deactivate",3)
end

function GDesIcon:Attach(newEntityId)
	GDes.DebugMsg("Entering Icon ["..self:GetIconId().."]:Attach",3)
	-- Hide the window and attach the Anchor to the target
	WindowSetShowing(self:GetWindow(),true)
	GDes.DebugMsg("Icon ["..self:GetIconId().."]:Attaching to World Object ["..newEntityId.."]",3)
	AttachWindowToWorldObject(self:GetAnchor(), newEntityId)
	self:SetAttachEntityId(newEntityId)
	self:SetIsAttached(true)
	GDes.DebugMsg("Leaving Icon ["..self:GetIconId().."]:Attach",3)
end

function GDesIcon:Detach()
	GDes.DebugMsg("Entering Icon ["..self:GetIconId().."]:Detach",3)
	-- Hide the window and detach the Anchor from the target
	if self:GetIsAttached() then
		WindowSetShowing(self:GetWindow(),false)
		DetachWindowFromWorldObject(self:GetAnchor(), self:GetAttachEntityId())
		self:SetAttachEntityId(0)
		self:SetIsAttached(false)
	end
	GDes.DebugMsg("Leaving Icon ["..self:GetIconId().."]:Detach",3)
end

function GDesIcon:UpdateTarget(targetDetails)
	GDes.DebugMsg("Entering Icon ["..self:GetIconId().."]:UpdateTarget",3)
	if not self:GetActive() or not self:GetTargetisOnline() then return false end
		
	-- Print Targets details for debugging purposes.
	GDes.DebugMsg("UT - -------- Target ["..self:GetIconId().."] values --------",2)
	GDes.DebugMsg(L"UT = Name ["..targetDetails.name..L"]",2)
	GDes.DebugMsg("UT - isDistant ["..boolToString(targetDetails.isDistant).."]",2)
	GDes.DebugMsg("UT - isInSameRegion ["..boolToString(targetDetails.isInSameRegion).."]",2)
	GDes.DebugMsg("UT - isonline ["..boolToString(targetDetails.online).."]",2)
	GDes.DebugMsg(L"UT - HealthPerc ["..targetDetails.healthPercent..L"]",2)
	GDes.DebugMsg(L"UT - World Object ID ["..towstring(targetDetails.worldObjNum)..L"]",2)
	GDes.DebugMsg("UT - -------- Target ["..self:GetIconId().."] values --------",2)

	-- Store the targets latest details.
	if self:GetTargetEntityId() ~= 0 or targetDetails.worldObjNum ~= 0 then
		self:SetTargetEntityIdOld(self:GetTargetEntityId())
	end
	
	self:SetTargetEntityId(targetDetails.worldObjNum)
	self:SetTargetisDistant(targetDetails.isDistant)
	self:SetTargetisInRegion(targetDetails.isInSameRegion)
	self:SetTargetisOnline(targetDetails.online)
	self:SetTargetHealthPerc(tonumber(tostring(targetDetails.healthPercent)))
	self:SetTargetisDead(self:GetTargetHealthPerc() == 0)
	
	if self:GetTargetCareerLine() == 0 then
		self:SetTargetCareerLine(targetDetails.careerLine)
		local newTexture, newOffsetX, newOffsetY = GetIconData(Icons.GetCareerIconIDFromCareerLine(self:GetTargetCareerLine()))
		self:SetTargetCareerTexture(newTexture)
		self:SetTargetCareerOffsetX(newOffsetX)
		self:SetTargetCareerOffsetY(newOffsetY)
	end

	if self:GetTargetEntityIdOld() ~= self:GetTargetEntityId() then
		self:Detach()
		if self:GetTargetEntityId() ~= 0 then
			self:Attach(self:GetTargetEntityId())
		elseif not self:GetTargetisDistant() then
			self:Attach(self:GetTargetEntityIdOld())
		end
	end
	
	self:UpdateTextures()
	GDes.DebugMsg("Leaving Icon ["..self:GetIconId().."]:UpdateTarget",3)
	return true
end

function GDesIcon:UpdateTextures()
	GDes.DebugMsg("Entering Icon ["..self:GetIconId().."]:UpdateTextures",3)
	-- Update Icon textures based on targets status
	self:SetImageSelection(GDes.Settings.Icons.DefaultIcon)
	self:SetStatusSelection(GDesConstants.FOREGROUND_BLANK)
	if self:GetTargetisDead() then
		self:SetImageSelection (GDes.Settings.Icons.DeadIcon)
		self:SetStatusSelection(GDesConstants.FOREGROUND_SKULL)
	elseif not self:GetTargetisOnline() then
		self:SetImageSelection (GDes.Settings.Icons.isOfflineIcon)
		self:SetStatusSelection(GDesConstants.FOREGROUND_TEXTDC)
	elseif GDes.Settings.Icons.Health.Enabled then
		GDes.DebugMsg(L"UT: Health Icon Enabled [true]",2)
		local targetHealth  = self:GetTargetHealthPerc()
		local thresholdHigh = tonumber(tostring(GDes.Settings.Icons.Health.HighPerc))
		local thresholdMed  = tonumber(tostring(GDes.Settings.Icons.Health.MedPerc))
		local thresholdLow  = tonumber(tostring(GDes.Settings.Icons.Health.MinPerc))
		if targetHealth > thresholdHigh then
			self:SetImageSelection(GDes.Settings.Icons.Health.FullIcon)
		elseif targetHealth > thresholdMed then
			self:SetImageSelection(GDes.Settings.Icons.Health.HighIcon)
		elseif targetHealth > thresholdLow then
			self:SetImageSelection(GDes.Settings.Icons.Health.MedIcon)
		else
			self:SetImageSelection(GDes.Settings.Icons.Health.MinIcon)
		end
	end
	self:SetTextures()
	GDes.DebugMsg("Leaving Icon ["..self:GetIconId().."]:UpdateTextures",3)
end

function GDesIcon:SetTextures()
	GDes.DebugMsg("Entering Icon ["..self:GetIconId().."]:SetTextures",3)
	-- Set the background texture and slice for the Icon
	DynamicImageSetTexture( self:GetImageName()
	                      , GDesConstants.IconsBackgrounds[self:GetImageSelection()].texture
	                      , 0, 0
	                      )
	DynamicImageSetTextureSlice( self:GetImageName()
	                           , GDesConstants.IconsBackgrounds[self:GetImageSelection()].slice
	                           )
	-- Set the foreground texture and slice for the Icon
	DynamicImageSetTexture( self:GetHilightName()
	                      , GDesConstants.IconsForegrounds[self:GetHilightSelection()].texture
	                      , 0, 0
	                      )
	DynamicImageSetTextureSlice( self:GetHilightName()
	                           , GDesConstants.IconsForegrounds[self:GetHilightSelection()].slice
	                           )
	-- Set the status texture and slice for the Icon
	DynamicImageSetTexture( self:GetStatusName()
	                      , GDesConstants.IconsForegrounds[self:GetStatusSelection()].texture
	                      , 0, 0
	                      )
	DynamicImageSetTextureSlice( self:GetStatusName()
	                           , GDesConstants.IconsForegrounds[self:GetStatusSelection()].slice
	                           )
	-- Show the Career Icon if it is enabled
	if self:GetExtrasEnabled(GDesConstants.EXTRAS_TARGET_CAREER) then
		GDes.DebugMsg("Icon ["..self:GetIconId().."]:ST: Career Icon Enabled",2)
		DynamicImageSetTexture( self:GetExtrasName(GDesConstants.EXTRAS_TARGET_CAREER)
		                      , self:GetTargetCareerTexture()
		                      , self:GetTargetCareerOffsetX()
		                      , self:GetTargetCareerOffsetY()
		                      )
	else
		DynamicImageSetTexture(self:GetExtrasName(GDesConstants.EXTRAS_TARGET_CAREER),"",0,0)
	end
	-- Show the Health Text if it is enabled
	if self:GetExtrasEnabled(GDesConstants.EXTRAS_TARGET_HEALTH) then
		GDes.DebugMsg("Icon ["..self:GetIconId().."]:ST: Health Text Enabled",2)
		LabelSetText(self:GetExtrasName(GDesConstants.EXTRAS_TARGET_HEALTH), towstring(self:GetTargetHealthPerc()))
	else
		LabelSetText(self:GetExtrasName(GDesConstants.EXTRAS_TARGET_HEALTH), L"")
	end
	-- Show the Target Name if it is enabled
	if self:GetExtrasEnabled(GDesConstants.EXTRAS_TARGET_NAME) then
		GDes.DebugMsg("Icon ["..self:GetIconId().."]:ST: Target Name Enabled",2)
		LabelSetText(self:GetExtrasName(GDesConstants.EXTRAS_TARGET_NAME), towstring(self:GetTargetName()))
	else
		LabelSetText(self:GetExtrasName(GDesConstants.EXTRAS_TARGET_NAME), L"")
	end
	GDes.DebugMsg("Leaving Icon ["..self:GetIconId().."]:SetTextures",3)
end
