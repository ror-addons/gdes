local EnableModCheckToggle           = false
local EnableButtonCheckToggle        = false
local EnableIconHealthCheckToggle    = false
local EnableExtraFeatureCheckToggle  = false
local EnableSoundCheckToggle         = false

local function stringToNumber(str)
	local val = tonumber(str)
	if type(val) ~= "number" then
		return 0
	end
	return val
end
-- GDes = {}
if GDes.Options == nil then
	GDes.Options = {}
	GDes.Options.Window  = "GDesOptions"

	GDes.Options.CurrentTab  = 1
	GDes.Options.Tabs = {
	   [GDesConstants.TAB_GENERAL_ID] = { name = GDes.Options.Window.."TabsGeneral" , label = "General" , altered = false }
	,  [GDesConstants.TAB_COLOUR_ID]  = { name = GDes.Options.Window.."TabsColour"  , label = "Colours" , altered = false }
	,  [GDesConstants.TAB_EXTRAS_ID]  = { name = GDes.Options.Window.."TabsExtras"  , label = "Extras"  , altered = false }
	                    }

	GDes.Options.General = {}
	GDes.Options.General.Window = GDes.Options.Window.."General"
	GDes.Options.General.Labels = {
	   { name = GDes.Options.General.Window.."ModLabel"           , value = "Enable the addon"}
	 , { name = GDes.Options.General.Window.."ToggleLabel"        , value = "Show the toggle button"}
	 , { name = GDes.Options.General.Window.."ToggleColourLabel"  , value = "Toggle button colour"}
	 , { name = GDes.Options.General.Window.."AlertsLabel"        , value = "Select an alert type"}
--	 , { name = GDes.Options.General.Window.."IconMaxLabel"       , value = "Maximum number of icons to display"}
	 , { name = GDes.Options.General.Window.."SoundEnableLabel"   , value = "Enable sounds"}
	 , { name = GDes.Options.General.Window.."SoundAppliedLabel"  , value = "Play sound when Guard is applied"}
	 , { name = GDes.Options.General.Window.."SoundRemovedLabel"  , value = "Play sound when Guard is removed"}
	                              }
	GDes.Options.Colour = {}
	GDes.Options.Colour.Window = GDes.Options.Window.."Colour"
	GDes.Options.Colour.Labels = {
	   { name = GDes.Options.Colour.Window.."SectionLabel"          , value = "Colour to display when target:"}
	 , { name = GDes.Options.Colour.Window.."DefaultColourLabel"    , value = "Is alive"}
	 , { name = GDes.Options.Colour.Window.."OfflineColourLabel"    , value = "Is disconnected"}
	 , { name = GDes.Options.Colour.Window.."DeathColourLabel"      , value = "Is dead"}
	 , { name = GDes.Options.Colour.Window.."HealthSectionLabel"    , value = "Colour to display when target:"}
	 , { name = GDes.Options.Colour.Window.."FullHealthColourLabel" , value = "Health is full"}
	 , { name = GDes.Options.Colour.Window.."HighHealthColourLabel" , value = "Health is high"}
	 , { name = GDes.Options.Colour.Window.."MedHealthColourLabel"  , value = "Health is medium"}
	 , { name = GDes.Options.Colour.Window.."MinHealthColourLabel"  , value = "Health is low"}
	 , { name = GDes.Options.Colour.Window.."HighHealthLabel"       , value = "High health threshold"}
	 , { name = GDes.Options.Colour.Window.."MedHealthLabel"        , value = "Medium health threshold"}
	 , { name = GDes.Options.Colour.Window.."MinHealthLabel"        , value = "Low health threshold"}
	 , { name = GDes.Options.Colour.Window.."IconHealthLabel"       , value = "Enable health tracking"}
	                           }

	GDes.Options.Extras = {}
	GDes.Options.Extras.Window = GDes.Options.Window.."Extras"
	GDes.Options.Extras.Labels = {
	  { name = GDes.Options.Extras.Window.."FeatureSectionLabel"   , value = "Add extra features to the icons:"}
	, { name = GDes.Options.Extras.Window.."FeatureSelectionLabel" , value = "Select which feature to add:"}
	, { name = GDes.Options.Extras.Window.."FeatureEnableLabel"    , value = "Enable the feature"}
	, { name = GDes.Options.Extras.Window.."FeaturePositionLabel"  , value = "Select feature's base position:"}
	, { name = GDes.Options.Extras.Window.."FeatureOffsetLabel"    , value = "Fine tune feature's poistion:"}
	, { name = GDes.Options.Extras.Window.."FeatureOffsetXLabel"   , value = "X:"}
	, { name = GDes.Options.Extras.Window.."FeatureOffsetYLabel"   , value = "Y:"}
	, { name = GDes.Options.Extras.Window.."IconsOffsetLabel"    , value = "Adjust icons poistion:"}
	, { name = GDes.Options.Extras.Window.."IconsOffsetXLabel"   , value = "X:"}
	, { name = GDes.Options.Extras.Window.."IconsOffsetYLabel"   , value = "Y:"}
	                            }
end

-------------------------------------------------
---------- Window Specific Functions ------------
-------------------------------------------------

------------------------------
--  Init Options Functions  --
------------------------------
function GDes.OptionsWindowInit()
	GDes.DebugMsg(L"Entering OptionsWindowInit",3)
	-- Create and register the options window and then hide it.
	CreateWindow(GDes.Options.Window, true)
	WindowSetShowing(GDes.Options.Window,false)
	GDes.DebugMsg(L"OptionsWindowInit: Window created",3)
	-- Set Title Bar text
	LabelSetText(GDes.Options.Window.."TitleBarText", L"GuarDee Enhancement System Options v"..towstring(GDes.Settings.Version))
	-- Set Tab text
	for i, v in ipairs(GDes.Options.Tabs) do
		ButtonSetText(v.name, towstring(v.label))
	end
	-- Set Button labels
	ButtonSetText(GDes.Options.Window.."SaveButton" , L"Save")
	ButtonSetText(GDes.Options.Window.."CloseButton", L"Close")
	-- Initialise tha tab windows
	GDes.OptionsInitGeneral()
	GDes.OptionsInitColour()
	GDes.OptionsInitExtras()
	GDes.Options.CurrentTab = GDesConstants.TAB_GENERAL_ID
	GDes.DebugMsg(L"Leaving OptionsWindowInit",3)
end

function GDes.OptionsInitGeneral()
	GDes.DebugMsg(L"Entering OptionsInitGeneral",3)
	-- Hide the General Tab Window until it is selected
	WindowSetShowing(GDes.Options.General.Window,false)
	-- Set the text in all of the labels for the General Tab Window
	for i,v in ipairs(GDes.Options.General.Labels) do
		LabelSetText(v.name,towstring(v.value))
	end
	for i, v in ipairs(GDesConstants.IconsBackgrounds) do
		ComboBoxAddMenuItem(GDes.Options.General.Window.."ToggleColourComboBox", towstring(v.name))
	end
	--Pre-populate the combo boxes with the available channels
	for i, v in pairs(GDesConstants.Alerts) do
		ComboBoxAddMenuItem(GDes.Options.General.Window.."AlertsComboBox", towstring(v.name))
	end
	--Pre-populate the combo boxes with the available sounds
	for i, v in pairs(GDesConstants.SoundTypes) do
		ComboBoxAddMenuItem(GDes.Options.General.Window.."SoundAppliedComboBox", towstring(v.name))
		ComboBoxAddMenuItem(GDes.Options.General.Window.."SoundRemovedComboBox", towstring(v.name))
	end
	--TextEditBoxSetAllowEditing(GDes.Options.General.Window.."IconMaxEditBox", false)
	GDes.DebugMsg(L"Leaving OptionsInitGeneral",3)
end

function GDes.OptionsInitColour()
	GDes.DebugMsg(L"Entering OptionsInitColour",3)
	-- Hide the Colour Tab Window until it is selected
	WindowSetShowing(GDes.Options.Colour.Window,false)
	-- Set the text in all of the labels for the Colour Tab Window
	for i,v in ipairs(GDes.Options.Colour.Labels) do
		LabelSetText(v.name,towstring(v.value))
	end
	-- Pre-populate the combo boxes with the available colours
	for i, v in ipairs(GDesConstants.IconsBackgrounds) do
		ComboBoxAddMenuItem(GDes.Options.Colour.Window.."DefaultColourComboBox"   , towstring(v.name))
		ComboBoxAddMenuItem(GDes.Options.Colour.Window.."OfflineColourComboBox"   , towstring(v.name))
		ComboBoxAddMenuItem(GDes.Options.Colour.Window.."DeathColourComboBox"     , towstring(v.name))
		ComboBoxAddMenuItem(GDes.Options.Colour.Window.."FullHealthColourComboBox", towstring(v.name))
		ComboBoxAddMenuItem(GDes.Options.Colour.Window.."HighHealthColourComboBox", towstring(v.name))
		ComboBoxAddMenuItem(GDes.Options.Colour.Window.."MedHealthColourComboBox" , towstring(v.name))
		ComboBoxAddMenuItem(GDes.Options.Colour.Window.."MinHealthColourComboBox" , towstring(v.name))
	end
	GDes.DebugMsg(L"Leaving OptionsInitColour",3)
end

function GDes.OptionsInitExtras()
	GDes.DebugMsg(L"Entering OptionsInitExtras",3)
	-- Hide the Extras Tab Window until it is selected
	WindowSetShowing(GDes.Options.Extras.Window,false)
	-- Set the text in all of the labels for the Extras Tab Window
	for i,v in ipairs(GDes.Options.Extras.Labels) do
		LabelSetText(v.name,towstring(v.value))
	end
	-- Pre-populate the combo box with the various feature types
	for i,v in ipairs(GDesConstants.Features) do
		ComboBoxAddMenuItem( GDes.Options.Extras.Window.."FeatureSelectionComboBox",towstring(v.name))
	end
	-- Pre-populate the combo box with the various position anchors types
	for i,v in ipairs(GDesConstants.Anchors) do
		ComboBoxAddMenuItem(GDes.Options.Extras.Window.."FeaturePositionComboBox", towstring(v.name))
	end
	GDes.DebugMsg(L"Leaving OptionsInitExtras",3)
end

---------------------------------
--   Show Settings functions   --
---------------------------------

function GDes.OptionsShow(flags, mouseX, mouseY)
	GDes.DebugMsg(L"Entering OptionsShow",3)
	-- Check if the General Tab is showing and if not, show it.
	if GDes.Options.CurrentTab ~= GDesConstants.TAB_GENERAL_ID then
		GDes.OptionsHideTab(GDes.Options.CurrentTab)
		GDes.Options.CurrentTab = GDesConstants.TAB_GENERAL_ID
	end
	GDes.OptionsShowTab(GDes.Options.CurrentTab)
	-- Now that all of the settings have been set, show the Options window.
	WindowSetShowing(GDes.Options.Window,true)
	GDes.DebugMsg(L"Leaving OptionsShow",3)
end

function GDes.OptionsShowTab(tabID)
	GDes.DebugMsg("Entering OptionsShowTab("..tabID..")",3)
	-- Loop through the tab buttons and set their pressed flag.
	for i, v in ipairs(GDes.Options.Tabs) do
		ButtonSetPressedFlag( v.name, i == tabID )
   end
	-- Check which tab needs to be shown and call the appropriate function
	if tabID == GDesConstants.TAB_GENERAL_ID then
		GDes.OptionsShowGeneral()
	elseif tabID == GDesConstants.TAB_COLOUR_ID then
		GDes.OptionsShowColour()
	elseif tabID == GDesConstants.TAB_EXTRAS_ID then
		GDes.OptionsShowExtras()
	end
	GDes.DebugMsg("Leaving OptionsShowTab("..tabID..")",3)
end

function GDes.OptionsHideTab(tabID)
	GDes.DebugMsg("Entering OptionsHideTab("..tabID..")",3)
	-- Check which tab window needs hiding and hide it.
	if tabID == GDesConstants.TAB_GENERAL_ID then
		WindowSetShowing(GDes.Options.General.Window,false)
	elseif tabID == GDesConstants.TAB_COLOUR_ID then
		WindowSetShowing(GDes.Options.Colour.Window,false)
	elseif tabID == GDesConstants.TAB_EXTRAS_ID then
		WindowSetShowing(GDes.Options.Extras.Window,false)
		GDes.OptionsExtrasReset()
	end
	GDes.DebugMsg("Leaving OptionsHideTab("..tabID..")",3)
end

function GDes.OptionsShowGeneral()
	GDes.DebugMsg(L"Entering OptionsShowGeneral",3)
	-- Enable Addon CheckBox Setting stored in GDes.Enabled
	EnableModCheckToggle = GDes.Settings.Enabled
	ButtonSetPressedFlag(GDes.Options.General.Window.."ModCheckBox",EnableModCheckToggle)
	-- Populate the toggle button settings to the latest saved
	EnableButtonCheckToggle = GDes.Settings.ShowButton
	ButtonSetPressedFlag(GDes.Options.General.Window.."ToggleCheckBox",EnableButtonCheckToggle)
	ComboBoxSetSelectedMenuItem(GDes.Options.General.Window.."ToggleColourComboBox",GDes.Settings.ToggleColour)
	-- Populate the Alert settings to the latest saved
	ComboBoxSetSelectedMenuItem(GDes.Options.General.Window.."AlertsComboBox", GDes.Settings.Alerts.Channel)
	-- Populate the maximum icon count setting to the latest saved
	--	TextEditBoxSetText(GDes.Options.General.Window.."IconMaxEditBox", towstring(GDes.Settings.Icons.MaxCount))
	-- Populate the Sound settings to the latest saved values
	EnableSoundCheckToggle = GDes.Settings.Sounds.Enabled
	ButtonSetPressedFlag(GDes.Options.General.Window.."SoundEnableCheckBox",EnableSoundCheckToggle)
	ComboBoxSetSelectedMenuItem(GDes.Options.General.Window.."SoundAppliedComboBox", GDes.Settings.Sounds.Applied)
	ComboBoxSetSelectedMenuItem(GDes.Options.General.Window.."SoundRemovedComboBox", GDes.Settings.Sounds.Removed)
	-- Settings populated, show the window.
	WindowSetShowing(GDes.Options.General.Window,true)
	GDes.DebugMsg(L"Leaving OptionsShowGeneral",3)
end

function GDes.OptionsShowColour()
	GDes.DebugMsg(L"Entering OptionsShowColour",3)
	ComboBoxSetSelectedMenuItem(GDes.Options.Colour.Window.."DefaultColourComboBox"   , GDes.Settings.Icons.DefaultIcon )
	ComboBoxSetSelectedMenuItem(GDes.Options.Colour.Window.."OfflineColourComboBox"   , GDes.Settings.Icons.isOfflineIcon )
	ComboBoxSetSelectedMenuItem(GDes.Options.Colour.Window.."DeathColourComboBox"     , GDes.Settings.Icons.DeadIcon )
	ComboBoxSetSelectedMenuItem(GDes.Options.Colour.Window.."FullHealthColourComboBox", GDes.Settings.Icons.Health.FullIcon )
	ComboBoxSetSelectedMenuItem(GDes.Options.Colour.Window.."HighHealthColourComboBox", GDes.Settings.Icons.Health.HighIcon )
	ComboBoxSetSelectedMenuItem(GDes.Options.Colour.Window.."MedHealthColourComboBox" , GDes.Settings.Icons.Health.MedIcon )
	ComboBoxSetSelectedMenuItem(GDes.Options.Colour.Window.."MinHealthColourComboBox" , GDes.Settings.Icons.Health.MinIcon )
	TextEditBoxSetText(GDes.Options.Colour.Window.."HighHealthEditBox", towstring(GDes.Settings.Icons.Health.HighPerc))
	TextEditBoxSetText(GDes.Options.Colour.Window.."MedHealthEditBox" , towstring(GDes.Settings.Icons.Health.MedPerc))
	TextEditBoxSetText(GDes.Options.Colour.Window.."MinHealthEditBox" , towstring(GDes.Settings.Icons.Health.MinPerc))
	-- Populate the Icon health trackking setting to the latest saved
	EnableIconHealthCheckToggle = GDes.Settings.Icons.Health.Enabled
	ButtonSetPressedFlag(GDes.Options.Colour.Window.."IconHealthCheckBox",EnableIconHealthCheckToggle)
	-- Show the window
	WindowSetShowing(GDes.Options.Colour.Window,true)
	GDes.DebugMsg(L"Leaving OptionsShowColour",3)
end

function GDes.OptionsShowExtras()
	GDes.DebugMsg(L"Entering OptionsShowExtras",3)
	-- Populate Extra Feature selection
	ComboBoxSetSelectedMenuItem(GDes.Options.Extras.Window.."FeatureSelectionComboBox", GDesConstants.EXTRAS_TARGET_HEALTH)
	GDes.ExtrasFeatureSelected( GDesConstants.EXTRAS_TARGET_HEALTH )
	-- Populate Icons Offset settings
	TextEditBoxSetText(GDes.Options.Extras.Window.."IconsOffsetXEditBox", towstring(GDes.Settings.Icons.OffsetX))
	TextEditBoxSetText(GDes.Options.Extras.Window.."IconsOffsetYEditBox", towstring(GDes.Settings.Icons.OffsetY))
	-- Show the window
	WindowSetShowing(GDes.Options.Extras.Window,true)
	GDes.DebugMsg(L"Leaving OptionsShowExtras",3)
end

---------------------------------
--  GUI Interaction functions  --
---------------------------------

function GDes.GeneralMaxIconPlus(flags, mouseX, mouseY)
	GDes.DebugMsg(L"Entering GeneralMaxIconPlus",3)
	local count = stringToNumber(tostring(TextEditBoxGetText(GDes.Options.General.Window.."IconMaxEditBox"))) + 1
	if count > GDesConstants.MAX_ICONS then count = GDesConstants.MAX_ICONS end
	TextEditBoxSetText(GDes.Options.General.Window.."IconMaxEditBox", towstring(count))
	GDes.DebugMsg(L"Leaving GeneralMaxIconPlus",3)
end

function GDes.GeneralMaxIconMinus(flags, mouseX, mouseY)
	GDes.DebugMsg(L"Entering GeneralMaxIconMinus",3)
	local count = stringToNumber(tostring(TextEditBoxGetText(GDes.Options.General.Window.."IconMaxEditBox"))) - 1
	if count < 0 then count = 0 end
	TextEditBoxSetText(GDes.Options.General.Window.."IconMaxEditBox", towstring(count))
	GDes.DebugMsg(L"Leaving GeneralMaxIconMinus",3)
end

function GDes.OptionsSelectTab(flags, mouseX, mouseY)
	GDes.DebugMsg(L"Entering OptionsSelectTab",3)
	local selectedTab = WindowGetId(SystemData.ActiveWindow.name)
	GDes.DebugMsg("OptionsSelectTab: SelectedTabID ["..selectedTab.."]",2)
	GDes.DebugMsg(L"OptionsSelectTab: CurrentTabID ["..GDes.Options.CurrentTab..L"]",2)
	-- Hide the deselected tab abd show the selected one
	GDes.OptionsHideTab(GDes.Options.CurrentTab)
	GDes.OptionsShowTab(selectedTab)
	-- Store which tab has been selected
	GDes.Options.CurrentTab = selectedTab
	GDes.DebugMsg(L"Leaving OptionsSelectTab",3)
end

function GDes.ExtrasFeatureSelected( selectionIndex )
	GDes.DebugMsg(L"Entering ExtrasFeatureSelected",3)
	local feature = GDes.Settings.Icons.Extras[selectionIndex]
	EnableExtraFeatureCheckToggle = feature.enabled
	ButtonSetPressedFlag(GDes.Options.Extras.Window.."FeatureEnableCheckBox",EnableExtraFeatureCheckToggle)
	ComboBoxSetSelectedMenuItem( GDes.Options.Extras.Window.."FeaturePositionComboBox", feature.anchor)
	TextEditBoxSetText(GDes.Options.Extras.Window.."FeatureOffsetXEditBox", towstring(feature.offsetX))
	TextEditBoxSetText(GDes.Options.Extras.Window.."FeatureOffsetYEditBox", towstring(feature.offsetY))
	GDes.DebugMsg(L"Leaving ExtrasFeatureSelected",3)
end

function GDes.ExtrasPositionSelected(selectionIndex)
	GDes.DebugMsg(L"Entering ExtrasPositionSelected",3)
	GDes.OptionsExtrasFeatureUpdate()
	GDes.DebugMsg(L"Leaving ExtrasPositionSelected",3)
end

function GDes.ExtrasFeatureOffsetUp(flags, mouseX, mouseY)
	GDes.DebugMsg(L"Entering ExtrasFeatureOffsetUp",3)
	local OffsetY = stringToNumber(tostring(TextEditBoxGetText(GDes.Options.Extras.Window.."FeatureOffsetYEditBox"))) - 1
	TextEditBoxSetText(GDes.Options.Extras.Window.."FeatureOffsetYEditBox", towstring(OffsetY))
	GDes.OptionsExtrasFeatureUpdate()
	GDes.DebugMsg(L"Leaving ExtrasFeatureOffsetUp",3)
end

function GDes.ExtrasFeatureOffsetDown(flags, mouseX, mouseY)
	GDes.DebugMsg(L"Entering ExtrasFeatureOffsetDown",3)
	local OffsetY = stringToNumber(tostring(TextEditBoxGetText(GDes.Options.Extras.Window.."FeatureOffsetYEditBox"))) + 1
	TextEditBoxSetText(GDes.Options.Extras.Window.."FeatureOffsetYEditBox", towstring(OffsetY))
	GDes.OptionsExtrasFeatureUpdate()
	GDes.DebugMsg(L"Leaving ExtrasFeatureOffsetDown",3)
end

function GDes.ExtrasFeatureOffsetLeft(flags, mouseX, mouseY)
	GDes.DebugMsg(L"Entering ExtrasFeatureOffsetLeft",3)
	local OffsetX = stringToNumber(tostring(TextEditBoxGetText(GDes.Options.Extras.Window.."FeatureOffsetXEditBox"))) - 1
	TextEditBoxSetText(GDes.Options.Extras.Window.."FeatureOffsetXEditBox", towstring(OffsetX))
	GDes.OptionsExtrasFeatureUpdate()
	GDes.DebugMsg(L"Leaving ExtrasFeatureOffsetLeft",3)
end

function GDes.ExtrasFeatureOffsetRight(flags, mouseX, mouseY)
	GDes.DebugMsg(L"Entering ExtrasFeatureOffsetRight",3)
	local OffsetX = stringToNumber(tostring(TextEditBoxGetText(GDes.Options.Extras.Window.."FeatureOffsetXEditBox"))) + 1
	TextEditBoxSetText(GDes.Options.Extras.Window.."FeatureOffsetXEditBox", towstring(OffsetX))
	GDes.OptionsExtrasFeatureUpdate()
	GDes.DebugMsg(L"Leaving ExtrasFeatureOffsetRight",3)
end

function GDes.ExtrasFeatureOffsetYKeyEntry(flags, mouseX, mouseY)
	GDes.DebugMsg(L"Entering ExtrasFeatureOffsetYKeyEntry",3)
	GDes.OptionsExtrasFeatureUpdate()
	GDes.DebugMsg(L"Leaving ExtrasFeatureOffsetYKeyEntry",3)
end

function GDes.ExtrasFeatureOffsetXKeyEntry(flags, mouseX, mouseY)
	GDes.DebugMsg(L"Entering ExtrasFeatureOffsetXKeyEntry",3)
	GDes.OptionsExtrasFeatureUpdate()
	GDes.DebugMsg(L"Leaving ExtrasFeatureOffsetXKeyEntry",3)
end

function GDes.ExtrasIconsOffsetUp(flags, mouseX, mouseY)
	GDes.DebugMsg(L"Entering ExtrasIconsOffsetUp",3)
	local OffsetX = 0
	local OffsetY = 0
	OffsetX = stringToNumber(tostring(TextEditBoxGetText(GDes.Options.Extras.Window.."IconsOffsetXEditBox")))
	OffsetY = stringToNumber(tostring(TextEditBoxGetText(GDes.Options.Extras.Window.."IconsOffsetYEditBox"))) - 1
	TextEditBoxSetText(GDes.Options.Extras.Window.."IconsOffsetYEditBox", towstring(OffsetY))
	GDes.ExtrasSetIconsAnchor(OffsetX, OffsetY)
	GDes.DebugMsg(L"Leaving ExtrasIconsOffsetUp",3)
end

function GDes.ExtrasIconsOffsetRight(flags, mouseX, mouseY)
	GDes.DebugMsg(L"Entering ExtrasIconsOffsetRight",3)
	local OffsetX = 0
	local OffsetY = 0
	OffsetX = stringToNumber(tostring(TextEditBoxGetText(GDes.Options.Extras.Window.."IconsOffsetXEditBox"))) + 1
	OffsetY = stringToNumber(tostring(TextEditBoxGetText(GDes.Options.Extras.Window.."IconsOffsetYEditBox")))
	TextEditBoxSetText(GDes.Options.Extras.Window.."IconsOffsetXEditBox", towstring(OffsetX))
	GDes.ExtrasSetIconsAnchor(OffsetX, OffsetY)
	GDes.DebugMsg(L"Leaving ExtrasIconsOffsetRight",3)
end

function GDes.ExtrasIconsOffsetLeft(flags, mouseX, mouseY)
	GDes.DebugMsg(L"Entering ExtrasIconsOffsetLeft",3)
	local OffsetX = 0
	local OffsetY = 0
	OffsetX = stringToNumber(tostring(TextEditBoxGetText(GDes.Options.Extras.Window.."IconsOffsetXEditBox"))) - 1
	OffsetY = stringToNumber(tostring(TextEditBoxGetText(GDes.Options.Extras.Window.."IconsOffsetYEditBox")))
	TextEditBoxSetText(GDes.Options.Extras.Window.."IconsOffsetXEditBox", towstring(OffsetX))
	GDes.ExtrasSetIconsAnchor(OffsetX, OffsetY)
	GDes.DebugMsg(L"Leaving ExtrasIconsOffsetLeft",3)
end

function GDes.ExtrasIconsOffsetDown(flags, mouseX, mouseY)
	GDes.DebugMsg(L"Entering ExtrasIconsOffsetDown",3)
	local OffsetX = 0
	local OffsetY = 0
	OffsetX = stringToNumber(tostring(TextEditBoxGetText(GDes.Options.Extras.Window.."IconsOffsetXEditBox")))
	OffsetY = stringToNumber(tostring(TextEditBoxGetText(GDes.Options.Extras.Window.."IconsOffsetYEditBox"))) + 1
	TextEditBoxSetText(GDes.Options.Extras.Window.."IconsOffsetYEditBox", towstring(OffsetY))
	GDes.ExtrasSetIconsAnchor(OffsetX, OffsetY)
	GDes.DebugMsg(L"Leaving ExtrasIconsOffsetDown",3)
end

function GDes.ExtrasIconsOffsetXKeyEntry(flags, mouseX, mouseY)
	GDes.DebugMsg(L"Entering ExtrasIconsOffsetXKeyEntry",3)
	local OffsetX = 0
	local OffsetY = 0
	OffsetX = stringToNumber(tostring(TextEditBoxGetText(GDes.Options.Extras.Window.."IconsOffsetXEditBox")))
	OffsetY = stringToNumber(tostring(TextEditBoxGetText(GDes.Options.Extras.Window.."IconsOffsetYEditBox")))
	GDes.ExtrasSetIconsAnchor(OffsetX, OffsetY)
	GDes.DebugMsg(L"Leaving ExtrasIconsOffsetXKeyEntry",3)
end

function GDes.ExtrasIconsOffsetYKeyEntry(flags, mouseX, mouseY)
	GDes.DebugMsg(L"Entering ExtrasIconsOffsetYKeyEntry",3)
	local OffsetX = 0
	local OffsetY = 0
	OffsetX = stringToNumber(tostring(TextEditBoxGetText(GDes.Options.Extras.Window.."IconsOffsetXEditBox")))
	OffsetY = stringToNumber(tostring(TextEditBoxGetText(GDes.Options.Extras.Window.."IconsOffsetYEditBox")))
	GDes.ExtrasSetIconsAnchor(OffsetX, OffsetY)
	GDes.DebugMsg(L"Leaving ExtrasIconsOffsetYKeyEntry",3)
end

function GDes.ExtrasSetIconsAnchor(newOffsetX, newOffsetY)
	GDes.DebugMsg(L"Entering ExtrasSetIconsAnchor",3)
	for k, a in pairs(GDes.Icons) do
		if k ~= GDesConstants.ICON_REFKEY then
			a:SetAnchor(newOffsetX, newOffsetY)
		end
	end
	GDes.DebugMsg(L"Leaving ExtrasSetIconsAnchor",3)
end

function GDes.OptionsExtrasFeatureUpdate()
	GDes.DebugMsg(L"Entering OptionsExtrasFeatureUpdate",3)
	local featureSelected  = ComboBoxGetSelectedMenuItem(GDes.Options.Extras.Window.."FeatureSelectionComboBox")
	local positionSelected = ComboBoxGetSelectedMenuItem(GDes.Options.Extras.Window.."FeaturePositionComboBox")
	local newOffsetX       = stringToNumber(tostring(TextEditBoxGetText(GDes.Options.Extras.Window.."FeatureOffsetXEditBox")))
	local newOffsetY       = stringToNumber(tostring(TextEditBoxGetText(GDes.Options.Extras.Window.."FeatureOffsetYEditBox")))
	-- Update the icons to reflect the user changes.
	for k, a in pairs(GDes.Icons) do
		if k ~= GDesConstants.ICON_REFKEY then
			a:SetExtrasEnabled (featureSelected, EnableExtraFeatureCheckToggle)
			a:SetExtrasAnchorId(featureSelected, positionSelected)
			a:SetExtrasOffsetX (featureSelected, newOffsetX)
			a:SetExtrasOffsetY (featureSelected, newOffsetY)
			a:SetExtrasAnchor(featureSelected)
			a:SetTextures()
		end
	end
	GDes.DebugMsg(L"Leaving OptionsExtrasFeatureUpdate",3)
end

function GDes.OptionsExtrasReset()
	GDes.DebugMsg(L"Entering OptionsExtrasReset",3)
	-- Reset the icons back to the previously saved version
	for k,a in pairs(GDes.Icons) do
		if k ~= GDesConstants.ICON_REFKEY then
			for g, e in pairs(GDes.Settings.Icons.Extras) do
				a:SetExtrasEnabled (g, e.enabled)
				a:SetExtrasAnchorId(g, e.anchor )
				a:SetExtrasOffsetX (g, e.offsetX)
				a:SetExtrasOffsetY (g, e.offsetY)
				a:SetExtrasAnchor  (g)
				a:SetTextures()
			end
			a:SetAnchor(GDes.Settings.Icons.OffsetX, GDes.Settings.Icons.OffsetY)
		end
	end
	GDes.DebugMsg(L"Leaving OptionsExtrasReset",3)
end

function GDes.OptionsSoundsPlay()
	GDes.DebugMsg(L"Entering OptionsSoundsPlay",3)
	local windowName, soundId
	if WindowGetId(SystemData.ActiveWindow.name) == 1 then
		windowName = "SoundAppliedComboBox"
	else
		windowName = "SoundRemovedComboBox"
	end
	local selection = ComboBoxGetSelectedMenuItem( GDes.Options.General.Window..windowName)
	if selection > 1 then
		Sound.Play(GDesConstants.SoundTypes[selection].id)
	end
	GDes.DebugMsg(L"Leaving OptionsSoundsPlay",3)
end

function GDes.OptionsClose(flags, mouseX, mouseY)
	GDes.DebugMsg(L"Entering OptionsClose",3)
	-- Hide the Options window
	WindowSetShowing(GDes.Options.Window,false)
	-- Re-apply to Extras settings in case the user altered but didn't save
	if GDes.Options.CurrentTab == GDesConstants.TAB_EXTRAS_ID then
		GDes.OptionsExtrasReset()
	end
	GDes.DebugMsg(L"Leaving OptionsClose",3)
end

function GDes.OptionsSave(flags, mouseX, mouseY)
	GDes.DebugMsg(L"Entering OptionsSave",3)
	-- Check which tab is shown and call the appropriate function
	if GDes.Options.CurrentTab == GDesConstants.TAB_GENERAL_ID then
		GDes.OptionsSaveGeneral()
	elseif GDes.Options.CurrentTab == GDesConstants.TAB_COLOUR_ID then
		GDes.OptionsSaveColour()
	elseif GDes.Options.CurrentTab == GDesConstants.TAB_EXTRAS_ID then
		GDes.OptionsSaveExtras()
	end
	GDes.UpdateTargetDetails()
	GDes.DebugMsg(L"Leaving OptionsSave",3)
end

---------------------------------
--   Save Settings functions   --
---------------------------------
function GDes.OptionsSaveGeneral()
	GDes.DebugMsg(L"Entering OptionsSaveGeneral",3)
	-- Check to see if the user has just changed the enable status of the mod and if so, alter the mod accordingly
	if (GDes.Enabled ~= GDes.Settings.Enabled) then GDes.Toggle() end
	GDes.Settings.Enabled = ButtonGetPressedFlag(GDes.Options.General.Window.."ModCheckBox")
	-- Store the toggle button settings and apply them
	GDes.Settings.ShowButton = ButtonGetPressedFlag(GDes.Options.General.Window.."ToggleCheckBox")
	GDes.Settings.ToggleColour = ComboBoxGetSelectedMenuItem(GDes.Options.General.Window.."ToggleColourComboBox")
	WindowSetShowing(GDes.Button.Window,GDes.Settings.ShowButton)
	GDes.ToggleButtonSetTextures()
	-- Store the Alert settings
	GDes.Settings.Alerts.Channel = ComboBoxGetSelectedMenuItem(GDes.Options.General.Window.."AlertsComboBox")
	GDes.Settings.Alerts.Enabled = GDes.Settings.Alerts.Channel ~= GDesConstants.ALERTS_DISABLED
--	GDes.Settings.Icons.MaxCount = stringToNumber(tostring(TextEditBoxGetText(GDes.Options.General.Window.."IconMaxEditBox")))
--	GDes.IconsUpdateMaxCount()
	GDes.Settings.Sounds.Enabled = ButtonGetPressedFlag(GDes.Options.General.Window.."SoundEnableCheckBox")
	GDes.Settings.Sounds.Applied = ComboBoxGetSelectedMenuItem(GDes.Options.General.Window.."SoundAppliedComboBox")
	GDes.Settings.Sounds.Removed = ComboBoxGetSelectedMenuItem(GDes.Options.General.Window.."SoundRemovedComboBox")
	GDes.DebugMsg(L"Leaving OptionsSaveGeneral",3)
end

function GDes.OptionsSaveColour()
	GDes.DebugMsg(L"Entering OptionsSaveColour",3)
	-- Store all of the values entered for the icons settings
	GDes.Settings.Icons.DefaultIcon     = ComboBoxGetSelectedMenuItem(GDes.Options.Colour.Window.."DefaultColourComboBox")
	GDes.Settings.Icons.isOfflineIcon   = ComboBoxGetSelectedMenuItem(GDes.Options.Colour.Window.."OfflineColourComboBox")
	GDes.Settings.Icons.DeadIcon        = ComboBoxGetSelectedMenuItem(GDes.Options.Colour.Window.."DeathColourComboBox")
	GDes.Settings.Icons.Health.FullIcon = ComboBoxGetSelectedMenuItem(GDes.Options.Colour.Window.."FullHealthColourComboBox")
	GDes.Settings.Icons.Health.HighIcon = ComboBoxGetSelectedMenuItem(GDes.Options.Colour.Window.."HighHealthColourComboBox")
	GDes.Settings.Icons.Health.MedIcon  = ComboBoxGetSelectedMenuItem(GDes.Options.Colour.Window.."MedHealthColourComboBox")
	GDes.Settings.Icons.Health.MinIcon  = ComboBoxGetSelectedMenuItem(GDes.Options.Colour.Window.."MinHealthColourComboBox")
	GDes.Settings.Icons.Health.HighPerc = stringToNumber(tostring(TextEditBoxGetText(GDes.Options.Colour.Window.."HighHealthEditBox")))
	GDes.Settings.Icons.Health.MedPerc  = stringToNumber(tostring(TextEditBoxGetText(GDes.Options.Colour.Window.."MedHealthEditBox")))
	GDes.Settings.Icons.Health.MinPerc  = stringToNumber(tostring(TextEditBoxGetText(GDes.Options.Colour.Window.."MinHealthEditBox")))
	GDes.Settings.Icons.Health.Enabled  = ButtonGetPressedFlag(GDes.Options.Colour.Window.."IconHealthCheckBox")
	GDes.DebugMsg(L"Leaving OptionsSaveColour",3)
end

function GDes.OptionsSaveExtras()
	GDes.DebugMsg(L"Entering OptionsSaveExtras",3)
	-- Save the extras settings in the icon to the settings vaariable for persistant storage.
	for k,a in pairs(GDes.Icons) do
		if k ~= GDesConstants.ICON_REFKEY then
			for g, e in pairs(GDes.Settings.Icons.Extras) do
				e.enabled = a:GetExtrasEnabled (g)
				e.anchor  = a:GetExtrasAnchorId(g)
				e.offsetX = a:GetExtrasOffsetX (g)
				e.offsetY = a:GetExtrasOffsetY (g)
			end
			-- We only need to do this with the first Icon we find.
			break
		end
	end
	GDes.Settings.Icons.OffsetX = stringToNumber(tostring(TextEditBoxGetText(GDes.Options.Extras.Window.."IconsOffsetXEditBox")))
	GDes.Settings.Icons.OffsetY = stringToNumber(tostring(TextEditBoxGetText(GDes.Options.Extras.Window.."IconsOffsetYEditBox")))
	GDes.DebugMsg(L"Leaving OptionsSaveExtras",3)
end

-----------------------
--- Misc. Functions ---
-----------------------
-- Can't workout how to keep check box ticked via API default functionality so here is a manual work around.
-- TODO: Locate the in-built in Mythic code to do this, it must exist somewhere.
function GDes.EnableModCheckToggle(flags, mouseX, mouseY)
	GDes.DebugMsg(L"Entering EnableModCheckToggle",3)
	EnableModCheckToggle = not EnableModCheckToggle
	ButtonSetPressedFlag(GDes.Options.General.Window.."ModCheckBox",EnableModCheckToggle)
	GDes.DebugMsg(L"Leaving EnableModCheckToggle",3)
end

function GDes.EnableButtonCheckToggle(flags, mouseX, mouseY)
	GDes.DebugMsg(L"Entering EnableButtonCheckToggle",3)
	EnableButtonCheckToggle = not EnableButtonCheckToggle
	ButtonSetPressedFlag(GDes.Options.General.Window.."ToggleCheckBox",EnableButtonCheckToggle)
	GDes.DebugMsg(L"Leaving EnableButtonCheckToggle",3)
end

function GDes.EnableSoundCheckToggle(flags, mouseX, mouseY)
	GDes.DebugMsg(L"Entering EnableSoundCheckToggle",3)
	EnableSoundCheckToggle = not EnableSoundCheckToggle
	ButtonSetPressedFlag(GDes.Options.General.Window.."SoundEnableCheckBox",EnableSoundCheckToggle)
	GDes.DebugMsg(L"Leaving EnableSoundCheckToggle",3)
end

function GDes.EnableIconHealthCheckToggle(flags, mouseX, mouseY)
	GDes.DebugMsg(L"Entering EnableIconHealthCheckToggle",3)
	EnableIconHealthCheckToggle = not EnableIconHealthCheckToggle
	ButtonSetPressedFlag(GDes.Options.Colour.Window.."IconHealthCheckBox",EnableIconHealthCheckToggle)
	GDes.DebugMsg(L"Leaving EnableIconHealthCheckToggle",3)
end

function GDes.EnableExtraFeatureCheckToggle(flags, mouseX, mouseY)
	GDes.DebugMsg(L"Entering EnableExtraFeatureCheckToggle",3)
	EnableExtraFeatureCheckToggle = not EnableExtraFeatureCheckToggle
	ButtonSetPressedFlag(GDes.Options.Extras.Window.."FeatureEnableCheckBox",EnableExtraFeatureCheckToggle)
	GDes.OptionsExtrasFeatureUpdate()
	GDes.DebugMsg(L"Leaving EnableExtraFeatureCheckToggle",3)
end
