<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="GDes" version="1.2" date="18/08/2010" >
		<Author name="Medikage" email="medikage@gmail.com" />
		<Description text="The GDes addon, places icons above the heads of tanks that are guarding you."/>		
		<VersionSettings gameVersion="1.3.6" windowsVersion="1.0" savedVariablesVersion="1.0" />
		<Dependencies>
			<Dependency name="LibSlash" />
			<Dependency name="EA_ChatWindow" />
			<Dependency name="EASystem_Utils"/>
			<Dependency name="EASystem_WindowUtils"/>
		</Dependencies>
		<Files>
			<File name="GDesConstants.lua" />
			<File name="GDes.xml" />
			<File name="GDesIcon.xml" />
			<File name="GDesOptions.xml" />
		</Files>
		<SavedVariables>
			<SavedVariable name="GDes.Settings" />
		</SavedVariables>
		<OnInitialize>
			<CallFunction name="GDes.Init" />
		</OnInitialize>
		<OnUpdate/>
		<OnShutdown>
			<CallFunction name="GDes.Shutdown" />
		</OnShutdown>
		<WARInfo>
			<Categories>
				<Category name="ACTION_BARS" />
			</Categories>
			<Careers>
				<Career name="BLACKGUARD" />
				<Career name="WITCH_ELF" />
				<Career name="DISCIPLE" />
				<Career name="SORCERER" />
				<Career name="IRON_BREAKER" />
				<Career name="SLAYER" />
				<Career name="RUNE_PRIEST" />
				<Career name="ENGINEER" />
				<Career name="BLACK_ORC" />
				<Career name="CHOPPA" />
				<Career name="SHAMAN" />
				<Career name="SQUIG_HERDER" />
				<Career name="WITCH_HUNTER" />
				<Career name="KNIGHT" />
				<Career name="BRIGHT_WIZARD" />
				<Career name="WARRIOR_PRIEST" />
				<Career name="CHOSEN" />
				<Career name="MARAUDER" />
				<Career name="ZEALOT" />
				<Career name="MAGUS" />
				<Career name="SWORDMASTER" />
				<Career name="SHADOW_WARRIOR" />
				<Career name="WHITE_LION" />
				<Career name="ARCHMAGE" />
			</Careers>
		</WARInfo>
	</UiMod>
</ModuleFile>
